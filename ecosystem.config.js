module.exports = {
    apps : [{
        name      : 'transactions',
        script    : 'yarn',
        args      : 'start',
        interpreter: '/bin/bash',
        env: {
            NODE_ENV: 'production'
        }
    }]
};
