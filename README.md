## Description
Backend para transacciones de pago [inStore](https://help.vtex.com/en/tracks/instore-getting-started-and-setting-up--zav76TFEZlAjnyBVL5tRc/1wtAanSRA3g2316dw7bw8u?&utm_source=autocomplete) respuestas basadas en la [documentación](https://developers.vtex.com/vtex-rest-api/docs/integrating-instore-to-a-new-payment-acquirer) de VTEX inStore [Configuración](https://developers.vtex.com/vtex-rest-api/docs/define-payment-methods-displayed-on-instore)

#### Documentos adicionales
- [Configuración inStore](https://docs.google.com/document/d/1uFPoTQg8QCc7CVznwcTAWchhgfN0Xf1mCPJJwbT64rE/edit#heading=h.xodfgo37t54a "Configuración inStore")
- [Registro de dispositivos](https://docs.google.com/document/d/1QV9UjckhCu_VT6H3e_OPUCA9H4_QmFyNd4lOfqU0Xyw/edit?usp=sharing "Registro de dispositivos")

## Servidor y DB
- Linux environment
- 2GB RAM
- 60GB De espacio en disco
- MongoDB para la base de datos

## Creación de transacciones

## Instalación

```bash
$ npm install
```

## Correr el proyecto
  
```bash

# development
$ npm run start


# watch mode
$ npm run start:dev


# production mode
$ npm run start:prod

```

#### Crear transacción de pago

El backend está conectado al API XML de REDEBAN, y permite realizar transacciones desde la web o usando la [aplicación móvil](https://gitlab.com/vtex-dev/instore-payment-north-latam/redebanapplink)

```http
  POST /api/transactions
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `orderId` | `string` | **Required**. OrderGroupID from inStore applink |
| `paymentId` | `string` | **Required**. paymentId from inStore applink |
| `transactionId` | `string` | **Required**. transactionId from inStore applink |
| `deviceId` | `string` | **Required**. ID for the device making the request, created trough the [admin app](https://gitlab.com/vtex-dev/instore-payment-north-latam/device-configuration) |
| `paymentType` | `string` | **Required**. webservice-redeban-pay for web transactions, deeplinking-redeban for app transactions|
| `payerEmail` | `string` | **Required**. Customer email |
| `payerIdentification` | `string` | **Required**. Customer identification |
| `amount` | `float` | **Required**. Transaction amount |
| `taxRate` | `float` | **Required**. Tax rate decimals ex. 0.19 |
| `debugMode` | `boolean` | **Required**. not implemented. default: true |

Response - webservice-redeban-pay payment type
```javascript
{
  "message": "Transaction created successfully",
  "data": {
    "createdAt": "2020-10-21T13:57:31.059Z",
    "updatedAt": "2020-10-21T13:57:31.059Z",
    "_id": "5f903e4bbae8acd8dcaa4741",
    "terminal": "SRB00387",
    "paymentUrl": "https://sipservicetest.azurewebsites.net/SIPService.asmx",
    "operation": "pay",
    "amount": 16000,
    "tip": 0,
    "tax": 2555,
    "invoice": "1070703455748",
    "taxReturnBase": 13445,
    "taxRateBase": 19,
    "cashierID": "01",
    "impoConsumo": 0,
    "impoConsumoBase": 0,
    "receipt": "8B837D24F131482585C854B953C3FEC6",
    "status": "pending",
    "response": {
      "_id": "5f903e4bbae8acd8dcaa4742",
      "rawResponse": "",
      "approveCode": "",
      "cardNumbers": "",
      "accountType": "",
      "franchise": "",
      "amount": 0,
      "taxes": 0,
      "impoConsumo": 0,
      "receiptNumber": "",
      "paymentID": "",
      "installments": "",
      "payeeTransactionId": "",
      "terminalNumber": "",
      "tenantNumber": "",
      "date": "",
      "hour": "",
      "voucherNumber": "",
      "operationType": ""
    },
    "responseAttempts": 0,
    "__v": 0
  }
}
```

Se debe guardar de la respuesta de "data._id" para usar este ID y crear un WebSocket listener donde el middleware enviará la respuesta de la transacción una vez finalice. listener de ejemplo para la respuesta de arriba:
```
"data._id": "5f903e4bbae8acd8dcaa4741"
```
El backend abrirá un canal de comunicación websocket para enviar el resultado de la transacción al data._id

Ejemplo de respuesta por WebSocket

```javascript
{
  "status": "rejected",
  "createdAt": "2020-10-21T13:57:31.045Z",
  "updatedAt": "2020-10-21T13:57:31.045Z",
  "_id": "5f903e4bbae8acd8dcaa4740",
  "orderId": "1070703455748",
  "paymentId": "8B837D24F131482585C854B953C3FEC6",
  "transactionId": "BE12ECF75C4242E7A54CD3CFE8642375",
  "paymentRequestId": "5f903e4bbae8acd8dcaa4741",
  "sellerName": "newbusinessunits",
  "sellerWhiteLabel": "newbusinessunits01",
  "deviceId": "5f8b87e0d1cf28cf36fc8ba4",
  "paymentDeviceId": "SRB00387",
  "paymentType": "webservice-redeban-pay",
  "payerEmail": "jamesm.osorio@gmail.com",
  "payerIdentification": "1128277367",
  "amount": 16000,
  "taxRate": 0.19,
  "debugMode": true,
  "__v": 0,
  "paymentResponse": {
    "terminal": "SRB00387",
    "paymentUrl": "https://sipservicetest.azurewebsites.net/SIPService.asmx",
    "operation": "pay",
    "amount": 16000,
    "tip": 0,
    "tax": 2555,
    "invoice": "1070703455748",
    "taxReturnBase": 13445,
    "taxRateBase": 19,
    "cashierID": "01",
    "impoConsumo": 0,
    "impoConsumoBase": 0,
    "receipt": "8B837D24F131482585C854B953C3FEC6",
    "status": "rejected",
    "response": {
      "rawResponse": "1,,514332**0182,00,,16000,255,134,0,000119,0,0,SRB00387,,201021,0857,0",
      "approveCode": "",
      "cardNumbers": "514332**0182",
      "accountType": "00",
      "franchise": "",
      "amount": "16000",
      "taxes": "255",
      "impoConsumo": "134",
      "receiptNumber": "0",
      "paymentID": "000119",
      "installments": "0",
      "payeeTransactionId": "0",
      "terminalNumber": "SRB00387",
      "tenantNumber": "",
      "date": "2020-10-21T08:57:00.000Z",
      "hour": "0857",
      "voucherNumber": "0",
      "operationType": "1"
    },
    "createdAt": "2020-10-21T13:57:31.059Z",
    "updatedAt": "2020-10-21T13:57:50.350Z"
  }
}
```
Luego de obtener la respuesta por parte del backend, se debe confirmar la transacción en el applink de [VTEX InStore](https://developers.vtex.com/vtex-rest-api/docs/integrating-instore-to-a-new-payment-acquirer):

#### Confirmar transacción

```javascript
  {
    "scheme": "instore",
    "action": "payment",
    "acquirerProtocol": "custom",
    "paymentId": "DB2995B1AEAD43869046DABE9BD11B69", // Igual al que envió instore
    "cardBrandName": "MASTER DEB", // franchise
    "acquirerName": "00", // accountType
    "firstDigits": "514332", // cardNumbers
    "lastDigits": "0182", // cardNumbers
    "tid": "5f8dca97bae8acd8dcaa4734", // _id de la respuesta
    "acquirerAuthorizationCode": "000117", // paymentID
    "nsu": "5f8dca97bae8acd8dcaa4733",// paymentRequestId de paymentResponse
    "merchantReceipt": "0", // receiptNumber
    "customerReceipt": "HOME01", // approveCode
    "responsecode": 0, // 0 si aprobado cualquier otro número sino -> operationType, si approveCode tiene algo está aprobado
    "reason": null
}
```



## All Endpoints

Methods
-------

#### [Auth](#Auth)

*   [`post /api/auth/token`](#authControllerGetSeller)
*   [`put /api/auth/token`](#authControllerUpdateToken)

#### [Devices](#Devices)

*   [`post /devices`](#deviceControllerAddDevice)
*   [`get /devices/count/{idSeller}`](#deviceControllerCountBySeller)
*   [`delete /devices/{idDevice}`](#deviceControllerDeleteDevice)
*   [`get /devices/{idDevice}`](#deviceControllerGetDevice)
*   [`get /devices`](#deviceControllerGetDevices)
*   [`get /devices/list/{sellerId}`](#deviceControllerGetDevicesBySeller)
*   [`get /devices/listbyname/{name}`](#deviceControllerGetDevicesBySellerName)
*   [`put /devices/{idDevice}`](#deviceControllerUpdateDevice)

#### [Sellers](#Sellers)

*   [`post /sellers`](#sellerControllerAddSeller)
*   [`delete /sellers/{idSeller}`](#sellerControllerDeleteSeller)
*   [`get /sellers/{idSeller}`](#sellerControllerGetSeller)
*   [`get /sellers/list/{idSetting}`](#sellerControllerGetSellersBySettings)
*   [`put /sellers/{idSeller}`](#sellerControllerUpdateSeller)

#### [Settings](#Settings)

*   [`post /settings`](#settingControllerAddSettings)
*   [`get /settings/{seller}`](#settingControllerSellerSettings)
*   [`put /settings/{seller}`](#settingControllerUpdateSettings)

#### [Transactions](#Transactions)

*   [`post /api/transactions`](#transactionControllerAddTransaction)
*   [`delete /api/transactions/{idTransaction}`](#transactionControllerDeleteTransaction)
*   [`get /api/transactions/payment/{idPaymentRequest}`](#transactionControllerGetPaymentRequest)
*   [`get /api/transactions/{idTransaction}`](#transactionControllerGetTransaction)
*   [`get /api/transactions`](#transactionControllerGetTransactions)
*   [`get /api/transactions/device/{idDevice}`](#transactionControllerGetTransactionsByDevice)
*   [`put /api/transactions/payment/alreadypaid/{idTransaction}`](#transactionControllerUpdatePaid)
*   [`put /api/transactions/{idTransaction}`](#transactionControllerUpdateTransaction)
*   [`patch /api/transactions`](#transactionControllerUpdateTransactions)

See swagger documentation or [swagger.json file](swagger.json)

```http
  GET /documentation
```

