import * as R from 'ramda';
import { ObjectId } from 'mongodb';

const isMongoId = R.is(ObjectId)

const isArray = R.is(Array)

const isObject = (x) => typeof x === 'object'

const isConvertible = (x) => x.toJSON !== undefined

const convertSingleObject = R.pipe(
  R.ifElse(isConvertible, (x) => x.toJSON(), R.identity),
  R.map(
    R.ifElse(isMongoId, (x: ObjectId) => x.toHexString(), (x) => {
      if(isConvertible(x)) {
        x = x.toJSON();
      }
      return R.ifElse(isArray, convertArray, (y) => {
        return R.ifElse(isObject, convertSingleObject, R.identity)(y)
      })(x)
    })
  )
);

const convertArray = R.pipe(
  R.map(convertSingleObject),
)

export const convertAllObjectIdToString = R.ifElse(
  isArray,
  convertArray,
  R.ifElse(isObject, convertSingleObject, R.identity)
);
