import * as R from "ramda";

export const isEmptyOrNil = R.either(R.isNil, R.isEmpty);

export const throwError = (exception: Error) => () => {
  throw exception;
};

export const throwErrorIfEmptyOrNil = (exception: Error) =>
  R.ifElse(isEmptyOrNil, throwError(exception), R.identity);