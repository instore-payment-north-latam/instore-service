import { Type } from '@nestjs/common';
import { plainToClass, ClassTransformOptions } from 'class-transformer';

export function mapper<T, V>(cls: Type<T>, plain: V[], options?: ClassTransformOptions): T[];
export function mapper<T, V>(cls: Type<T>, plain: V, options?: ClassTransformOptions): T;
export function mapper<T, V>(cls: Type<T>, plain: V | V[], options: ClassTransformOptions = { excludeExtraneousValues: true }): T {
  return plainToClass(cls, plain, options);
}
