import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  PlainLiteralObject,
  Type
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { mapper } from '../mapper.utils';
import { convertAllObjectIdToString } from '../object-id.utils';

@Injectable()
export class SerializerInterceptor implements NestInterceptor {

  constructor(
    private readonly dtoClass: Type<any>,
    private readonly message: string
  ) { }

  intercept(_: ExecutionContext, next: CallHandler<any>): Observable<any> | Promise<Observable<any>> {
    return next.handle().pipe(
      map((data: PlainLiteralObject | Array<PlainLiteralObject>) =>
        this.serialize(data)
      )
    );
  }

  private serialize(data: PlainLiteralObject | Array<PlainLiteralObject>): any {
    const result = Array.isArray(data)
      ? (data).map(item => mapper(this.dtoClass, convertAllObjectIdToString(item)))
      : mapper(this.dtoClass, convertAllObjectIdToString(data));
    return {
      message: this.message,
      data: result
    }
  }

}
