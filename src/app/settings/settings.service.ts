import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Setting } from "../interfaces/settings.interface";
import { SettingsDto } from "../dto/setting/settings.dto";

@Injectable()
export class SettingService {
    constructor(@InjectModel('Setting') private readonly settingModel: Model<Setting>) {}

    async addSettings(createSettingsDTO: SettingsDto): Promise<Setting> {
        const newConfig = await new this.settingModel(createSettingsDTO);
        return newConfig.save();
    }

    async getSettings(seller): Promise<Setting> {
        const config = await this.settingModel
            .find({
                seller
            })
            .exec()
        return config[0]
    }

    async getSettingsById(id): Promise<Setting> {
        const config = await this.settingModel
            .findById(id)
            .exec()
        return config
    }

    /*TODO: Fix error No overload matches this call */
    async updateSettings(seller, createSettingsDTO: SettingsDto): Promise<Setting> {
        const config = await this.settingModel
            .findOneAndUpdate({
                seller
                /* @ts-ignore */
            }, createSettingsDTO, { new: true })
        return config
    }
}
