import { Module } from '@nestjs/common';
import { SettingService } from './settings.service';
import { SettingController } from './settings.controller';
import { MongooseModule } from "@nestjs/mongoose";
import { SettingsSchema } from "../schemas/settings.schema";
import { SellerService } from "../seller/seller.service";
import { SellersSchema } from "../schemas/sellers.schema";
import { SellerController } from "../seller/seller.controller";
import { DevicesSchema } from "../schemas/device.schema";
import { DeviceService } from "../device/device.service";
import { DeviceController } from "../device/device.controller";
import { AuthModule } from '../auth/auth.module';


@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Setting', schema: SettingsSchema },
      { name: 'Seller', schema: SellersSchema },
      { name: 'Device', schema: DevicesSchema }
    ]),
    AuthModule
  ],
  providers: [SettingService, SellerService, DeviceService],
  controllers: [SettingController, SellerController, DeviceController],
  exports: [SettingService, SellerService, DeviceService]
})
export class SettingModule {}
