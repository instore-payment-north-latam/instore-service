import { Controller, Get, Res, HttpStatus, Param, NotFoundException, Post, Body, Put, UseGuards } from '@nestjs/common';
import { SettingService } from "./settings.service";
import { SettingsDto } from "../dto/setting/settings.dto";
import * as moment from "moment";
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiParam, ApiResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { NotFoundExceptionDto } from '../dto/shared/not-found-exception.dto';
import { UnauthorizedExceptionDto } from '../dto/shared/unauthorized-exception.dto';
 /**
  * @todo add auth
  */
// @UseGuards(AuthGuard())
@ApiTags('Settings')
@ApiBearerAuth()
@ApiUnauthorizedResponse({ description: "Unauthorized", type: UnauthorizedExceptionDto })
@Controller('settings')
export class SettingController {
    constructor(private settingService: SettingService) { }
    
    @Post('/')
    @ApiResponse({ status: HttpStatus.OK, type: SettingsDto, description: "Config saved" })
    async addSettings(@Res() res, @Body() createSettingsDTO: SettingsDto) {
        const newConfig = await this.settingService.addSettings(createSettingsDTO)
        return res.status(HttpStatus.OK).json({
            message: 'Config saved',
            data: newConfig,
        });
    }

    @Get('/:seller')
    @ApiParam({ name: 'seller' })
    @ApiResponse({ status: HttpStatus.OK, type: SettingsDto, description: "config retrieved"})
    @ApiResponse({ status: HttpStatus.NOT_FOUND, type: NotFoundExceptionDto, description: "Config not found" })
    async sellerSettings(@Res() res, @Param('seller') seller: string) {
        const data = await this.settingService.getSettings(seller);
        if (!data) {
            throw new NotFoundException('Config not found')
        }
        return res.status(HttpStatus.OK).json({
            message: 'config retrieved',
            data
        })
    }

    @Put('/:seller')
    @ApiParam({ name: 'seller' })
    @ApiResponse({ status: HttpStatus.OK, type: SettingsDto, description: "config updated" })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, type: NotFoundExceptionDto, description: "Config not found" })
    async updateSettings(@Res() res, @Param('seller') seller: string, @Body() createSettingsDTO: SettingsDto) {
        createSettingsDTO.updatedAt = moment().toISOString()
        const config = await this.settingService.updateSettings(seller, createSettingsDTO)
        if (!config) {
            throw new NotFoundException('Config not found')
        }

        return res.status(HttpStatus.OK).json({
            message: 'config updated',
            data: config
        })
    }
}
