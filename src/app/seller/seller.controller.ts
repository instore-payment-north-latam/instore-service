import { Controller, Get, Res, HttpStatus, Param, NotFoundException, Post, Body, Put, Query, Delete, UseGuards } from '@nestjs/common';
import { SellerService } from "./seller.service";
import { SellerDto } from "../dto/seller/seller.dto";
import { ValidateObjectId } from "../shared/pipes/validate-object-id.pipes";
import * as moment from 'moment';
import { DeviceService } from '../device/device.service';
import { Utils } from '../shared/utils';
import { ApiBearerAuth, ApiParam, ApiQuery, ApiResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { RequestPaginationDto } from '../dto/shared/request-pagination.dto';
import { ResponseSellersDto } from '../dto/seller/response-sellers.dto';
import { NotFoundExceptionDto } from '../dto/shared/not-found-exception.dto';
import { UnauthorizedExceptionDto } from '../dto/shared/unauthorized-exception.dto';
/**
 * @todo Add auth
 */
// @UseGuards(AuthGuard())
@ApiTags('Sellers')
@ApiBearerAuth()
@ApiUnauthorizedResponse({ description: "Unauthorized", type: UnauthorizedExceptionDto })
@Controller('sellers')
export class SellerController {
    constructor(
        private sellerService: SellerService,
        private readonly deviceService: DeviceService) { }

    @Post('/')
    @ApiResponse({ status: HttpStatus.OK, type: SellerDto, description: "Seller saved" })
    @ApiResponse({ status: HttpStatus.BAD_REQUEST, type: NotFoundExceptionDto, description: "The seller (seller) already exists" })
    async addSeller(@Res() res, @Body() createSellerDTO: SellerDto) {
        const newSeller = await this.sellerService.createSeller(createSellerDTO)
        return res.status(HttpStatus.OK).json({
            message: 'Seller saved',
            data: newSeller,
        })
    }

    @Get('/:idSeller')
    @ApiParam({ name: 'idSeller' })
    @ApiResponse({ status: HttpStatus.OK, type: SellerDto, description: "seller retrieved" })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, type: NotFoundExceptionDto, description: "Seller not found" })
    async getSeller(@Res() res, @Param('idSeller', new ValidateObjectId()) idSeller: string) {
        const seller = await this.sellerService.getSellerId(idSeller);
        if (!seller) {
            throw new NotFoundException('Seller not found')
        }
        return res.status(HttpStatus.OK).json({
            message: 'seller retrieved',
            data: seller
        })
    }

    @Get('list/:idSetting')
    @ApiParam({ name: "idSetting" })
    @ApiQuery({ name: "number-page", required: false })
    @ApiQuery({ name: "per-page", required: false })
    @ApiQuery({ name: "name", required: false })
    @ApiResponse({ status: HttpStatus.OK, type: ResponseSellersDto, description: "sellers retrieved" })
    async getSellersBySettings(
        @Res() res,
        @Param('idSetting', new ValidateObjectId()) idSetting: string,
        @Query() query: RequestPaginationDto
    ) {

        /** Si no se especifica el numero de registros entonces por default son 10  */
        const perPage = +(query["per-page"] || 10);

        /** Si no se especifica el numero de la pagina entonces la pagina primera pagina es seleccionada  */
        const numberPage = +(query["number-page"] || 1);

        const sellerPaginationResult = await this.sellerService.getSellersByPagination(
            idSetting,
            numberPage,
            perPage,
            Utils.clearText(query.name || ""),
        );

        const sellersFilters = await this.deviceService.getCountBySeller(sellerPaginationResult.docs);

        return res.status(HttpStatus.OK).json({
            message: 'sellers retrieved',
            data: {
                sellers: sellersFilters,
                pagination: {
                    currentPage: sellerPaginationResult.page,
                    pages: sellerPaginationResult.totalPages,
                    total: sellerPaginationResult.totalDocs,
                    perPage: sellerPaginationResult.limit
                }
            }
        })
    }

    @Put('/:idSeller')
    @ApiParam({ name: 'idSeller' })
    @ApiResponse({ status: HttpStatus.OK, type: SellerDto, description: "Seller updated" })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, type: NotFoundExceptionDto, description: "Seller not found" })
    async updateSeller(@Res() res, @Body() createSellerDTO: SellerDto, @Param('idSeller', new ValidateObjectId()) id: string) {
        delete createSellerDTO._configID
        createSellerDTO.updatedAt = moment().toISOString()

        const update = await this.sellerService.updateSeller(id, createSellerDTO);

        if (!update) {
            throw new NotFoundException('Seller not found');
        }

        return res.status(HttpStatus.OK).json({
            message: 'Seller updated',
            data: update
        });
    }

    @Delete('/:idSeller')
    @ApiParam({ name: 'idSeller' })
    @ApiResponse({ status: HttpStatus.OK, description: "Seller deleted", type: SellerDto })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, type: NotFoundExceptionDto, description: "Seller not found!" })
    async deleteSeller(@Res() res, @Param('idSeller', new ValidateObjectId()) idSeller: string) {
        const deleted = await this.sellerService.deleteSeller(idSeller)
        if (!deleted) {
            throw new NotFoundException('Seller not found!');
        }
        return res.status(HttpStatus.OK).json({
            message: 'Seller deleted',
            data: deleted
        });
    }
}
