import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ModelSeller, Seller } from "../interfaces/seller.interface";
import { SellerDto } from "../dto/seller/seller.dto";
import { PaginateResult } from '../utils/mongoose-paginate-v2.util';

@Injectable()
export class SellerService {
    constructor(@InjectModel('Seller') private readonly sellerModel: ModelSeller<Seller>) { }

    async createSeller(createSellerDTO: SellerDto): Promise<Seller> {
        await this.existsSeller(createSellerDTO.seller);

        const newSeller = await new this.sellerModel(createSellerDTO);
        return newSeller.save();
    }

    private async existsSeller(seller: string) {
        const sellerData = await this.getSeller(seller);
        if (sellerData._id) {
            throw new BadRequestException(`The seller (${seller}) already exists`);
        }
    }

    async getSellerId(id): Promise<Seller> {
        const seller = await this.sellerModel
            .findById(id)
            .exec()
        return seller
    }

    async getSeller(name): Promise<Seller> {
        const seller = await this.sellerModel
            .findOne({
                seller: name
            })
            .exec();

        return seller;
    }

    async getSettingSellers(idSetting: string): Promise<Seller[]> {
        const sellers = await this.sellerModel.find({
            _configID: idSetting
        })
            .exec();
        return sellers;
    }

    async getSellersByPagination(configId: string, pageNumber = 1, numberPerPage: number, textFilter: string): Promise<PaginateResult<Seller>> {

        return await this.sellerModel
            .paginate
            ({
                $and: [
                    { _configID: configId },
                    {
                        $or: [
                            { seller: new RegExp(`.*${textFilter}.*`, "i") },
                            { sellerName: new RegExp(`.*${textFilter}.*`, "i") }
                        ]
                    }
                ]
            }, {
                page: pageNumber, limit: numberPerPage
            });
    }

    async updateSeller(id, createSellerDTO: SellerDto): Promise<Seller> {
        const seller = await this.sellerModel
            .findByIdAndUpdate(id, createSellerDTO, { new: true })
            .exec();
        return seller;
    }

    async deleteSeller(id): Promise<any> {
        const deleted = await this.sellerModel
            .findByIdAndRemove(id);
        return deleted
    }

}
