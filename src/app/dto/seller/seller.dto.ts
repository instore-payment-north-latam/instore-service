import { ApiProperty, ApiResponseProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";
import { IsDefined, IsMongoId, IsNotEmpty, IsNumber, IsOptional, IsString, ValidateNested } from "class-validator";

/******************* Dirección *******************/
class Location {
    @Expose()
    @IsNumber()
    @ApiProperty()
    readonly latitude: number;

    @Expose()
    @IsNumber()
    @ApiProperty()
    readonly longitude: number;
}
class Address {
    @Expose()
    @IsString()
    @ApiProperty()
    readonly country: string;

    @Expose()
    @IsString()
    @ApiProperty()
    readonly state: string;

    @Expose()
    @IsString()
    @ApiProperty()
    readonly city: string;

    @Expose()
    @IsString()
    @ApiProperty()
    readonly complement: string;

    @Expose()
    @ValidateNested()
    @ApiProperty()
    readonly location: Location;

    @Expose()
    @IsString()
    @ApiProperty()
    readonly neighborhood: string;

    @Expose()
    @IsString()
    @ApiProperty()
    readonly number: string;

    @Expose()
    @IsString()
    @ApiProperty()
    readonly postalCode: string;

    @Expose()
    @IsString()
    @ApiProperty()
    readonly street: string;
}
/******************* Medios de pago *******************/
class Redeban {
    @Expose()
    @IsString()
    @ApiProperty()
    readonly model: string;

    @Expose()
    @IsString()
    @ApiProperty()
    readonly terminal: string;

    @Expose()
    @IsString()
    @ApiProperty()
    readonly url: string;

}

class MercadoPago {
    @Expose()
    @IsString()
    @ApiProperty()
    readonly client_id: string;

    @Expose()
    @IsString()
    @ApiProperty()
    readonly client_secret: string;
}
class PaymentMethods {
    @Expose()
    @ValidateNested()
    @ApiProperty()
    readonly redeban: Redeban;

    @Expose()
    @ValidateNested()
    @ApiProperty()
    readonly mercadopago: MercadoPago;
}


export class SellerDto {

    @ApiResponseProperty()
    _id?: string;

    @Expose()
    @IsString()
    @IsDefined()
    @IsNotEmpty()
    @ApiProperty()
    @IsMongoId()
    _configID: string;

    @Expose()
    @IsString()
    @IsDefined()
    @IsNotEmpty()
    @ApiProperty()
    readonly seller: string;

    @Expose()
    @IsString()
    @IsDefined()
    @IsNotEmpty()
    @ApiProperty()
    readonly sellerName: string;

    @Expose()
    @IsString()
    @ApiProperty()
    @IsOptional()
    readonly pickupPointID?: string;

    @Expose()
    @IsDefined()
    @ApiProperty()
    @ValidateNested()
    readonly address: Address;

    @Expose()
    @IsDefined()
    @ValidateNested()
    @ApiProperty()
    readonly paymentMethods: PaymentMethods;

    @Expose()
    @IsString()
    @IsOptional()
    @ApiProperty()
    readonly createdAt?: string;

    @Expose()
    @IsString()
    @IsOptional()
    @ApiProperty()
    updatedAt?: string;
}
