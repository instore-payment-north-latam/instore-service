import { ApiProperty } from "@nestjs/swagger";
import { SellerDto } from "./seller.dto";
import { ResponsePaginationDto } from "../shared/response-pagination.dto";



export class ResponseSellersDto {
    @ApiProperty({ isArray: true })
    readonly sellers: SellerDto;

    @ApiProperty()
    pagination: ResponsePaginationDto;

}