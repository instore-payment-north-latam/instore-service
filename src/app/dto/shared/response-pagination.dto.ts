import { ApiProperty } from "@nestjs/swagger";
import { IsNumber } from "class-validator";

export class ResponsePaginationDto {
    @ApiProperty()
    @IsNumber()
    readonly currentPage: number;

    @ApiProperty()
    @IsNumber()
    readonly pages: number;

    @ApiProperty()
    @IsNumber()
    readonly total: number;

    @ApiProperty()
    @IsNumber()
    readonly perPage: number;
}