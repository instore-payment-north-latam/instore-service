import { ApiProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";
import { IsNumber, IsOptional, IsString } from "class-validator";

export class RequestPaginationDto {

    @Expose()
    @IsNumber()
    @IsOptional()
    @ApiProperty({ required: false })
    readonly ["number-page"]?: number;

    @Expose()
    @IsNumber()
    @IsOptional()
    @ApiProperty({ required: false })
    readonly ["per-page"]?: number;

    /**
     * Property for find a element
     */
    @Expose()
    @IsString()
    @IsOptional()
    @ApiProperty({ required: false, description: "Filtrar por nombre" })
    readonly name: string;
}