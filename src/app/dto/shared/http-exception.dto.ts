import { ApiProperty } from "@nestjs/swagger";

export class HttpExceptionDto {
    @ApiProperty()
    statusCode: string;

    @ApiProperty()
    message: string;

    @ApiProperty({ required: false })
    error?: string;
}