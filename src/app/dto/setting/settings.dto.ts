import { ApiProperty, ApiResponseProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";
import { IsBoolean, IsDefined, IsNotEmpty, IsNumber, IsOptional, IsString, ValidateNested } from "class-validator";

/**
 * @todo Validate when send a string "false"
 */
class Redeban {
    @ApiProperty()
    @Expose()
    @IsBoolean()
    readonly active: boolean;

    @ApiProperty()
    @Expose()
    @IsBoolean()
    @IsOptional()
    readonly url?: boolean;
}

/**
 * @todo Validate when send a string "false"
 */
class Credibanco {

    @ApiProperty()
    @Expose()
    @IsBoolean()
    readonly active: boolean;

    @ApiProperty()
    @Expose()
    @IsBoolean()
    @IsOptional()
    readonly url?: boolean;
}

/**
 * @todo Validate when send a string "false"
 */
class MercadoPago {

    @ApiProperty()
    @Expose()
    @IsBoolean()
    readonly active: boolean;

    @ApiProperty()
    @Expose()
    @IsString()
    readonly client_id: string;

    @ApiProperty()
    @Expose()
    @IsString()
    readonly client_secret: string;
}

/**
 * @todo Validate when send a string "false"
 */
class Bancolombia {

    @ApiProperty()
    @Expose()
    @IsBoolean()
    readonly active: boolean;
}

/**
 * @todo Validate when send a string "false"
 */
class NequiInterface {

    @ApiProperty()
    @Expose()
    @IsBoolean()
    readonly active: boolean;
}

/**
 * @todo Validate when send a string "false"
 */
class DaviPlata {

    @ApiProperty()
    @Expose()
    @IsBoolean()
    readonly active: boolean;
}

class PaymentMethods {

    @ApiProperty({ required: false })
    @Expose()
    @ValidateNested()
    @IsOptional()
    readonly redeban?: Redeban;

    @ApiProperty({ required: false })
    @Expose()
    @ValidateNested()
    @IsOptional()
    readonly credibanco?: Credibanco;

    @ApiProperty({ required: false })
    @Expose()
    @ValidateNested()
    @IsOptional()
    readonly mercadopago?: MercadoPago;

    @ApiProperty({ required: false })
    @Expose()
    @ValidateNested()
    @IsOptional()
    readonly bancolombia?: Bancolombia;

    @ApiProperty({ required: false })
    @Expose()
    @ValidateNested()
    @IsOptional()
    readonly daviplata?: DaviPlata;
}


export class SettingsDto {

    @ApiResponseProperty()
    readonly _id?: string;

    @Expose()
    @ApiProperty()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly seller: string;

    @Expose()
    @ApiProperty()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly active: string;

    @Expose()
    @ApiProperty()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly sellerName: string;

    @Expose()
    @ApiProperty()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly sellerTaxID: string;

    @Expose()
    @ApiProperty()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly country: string;

    @Expose()
    @ApiProperty()
    @IsDefined()
    @IsNotEmpty()
    @IsNumber()
    readonly taxRate: number;

    @Expose()
    @ApiProperty()
    @IsDefined()
    @ValidateNested()
    readonly paymentMethods: PaymentMethods;

    @Expose()
    @ApiProperty()
    @IsString()
    readonly createdAt: string;

    @Expose()
    @ApiProperty()
    @IsString()
    updatedAt: string;
}
