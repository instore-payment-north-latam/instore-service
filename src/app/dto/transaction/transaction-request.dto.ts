import { IsBoolean, IsDefined, IsEnum, IsNotEmpty, IsNumber, IsString } from "class-validator";
import { Transaction } from "../../shared/enums";
import { ApiProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";

export class TransactionRequestDto {

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly orderId: string;

    @ApiProperty({ description: "Enviado por (vtex)" })
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly paymentId: string;

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly transactionId: string;

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly deviceId: string;

    @ApiProperty({ enum: Transaction.PAYMENT_TYPE })
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsEnum(Transaction.PAYMENT_TYPE)
    readonly paymentType: Transaction.PAYMENT_TYPE;

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly payerEmail: string;

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly payerIdentification: string;

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsNumber()
    readonly amount: number;

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsNumber()
    readonly taxRate: number;

    /**
     * @todo Validate when send a string "false"
     */
    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsBoolean()
    readonly debugMode: boolean;

}
