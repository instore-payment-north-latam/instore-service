import { ApiProperty, ApiResponseProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";
import { IsBoolean, IsDefined, IsEnum, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import { Transaction } from "../../shared/enums";

export class TransactionDto {

    @ApiResponseProperty()
    readonly _id?: string;

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly orderId: string;

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly paymentId: string;

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly transactionId: string;

    @Expose()
    @ApiResponseProperty()
    paymentRequestId: string;

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly sellerName: string;

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly sellerWhiteLabel: string;

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly deviceId: string;

    @ApiProperty({ required: false })
    @Expose()
    @IsOptional()
    @IsString()
    readonly paymentDeviceId?: string;

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsEnum(Transaction.PAYMENT_TYPE)
    readonly paymentType: Transaction.PAYMENT_TYPE;

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly payerEmail: string;

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly payerIdentification: string;

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsNumber()
    readonly amount: number;

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsNumber()
    readonly taxRate: number;

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsEnum(Transaction.STATUS)
    readonly status: Transaction.STATUS;

    /**
     * @todo Validate when is "false"
     */
    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsBoolean()
    readonly debugMode: boolean;

    @ApiProperty()
    @Expose()
    @IsString()
    @IsOptional()
    readonly createdAt?: string;

    @ApiProperty()
    @Expose()
    @IsString()
    @IsOptional()
    updatedAt?: string;

    @Expose()
    @ApiResponseProperty()
    @IsOptional()
    isConfirmed: boolean;
}
