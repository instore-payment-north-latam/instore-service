import { ApiProperty } from "@nestjs/swagger";
import { PaymentRequestDto } from "./create-payment-request.dto";
import { TransactionDto } from "./transaction.dto";

export class ResponseTransactionDto extends TransactionDto {
    @ApiProperty()
    paymentResponse: PaymentRequestDto
}