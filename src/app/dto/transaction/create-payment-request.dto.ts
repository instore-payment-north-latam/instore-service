import { ApiProperty } from "@nestjs/swagger";
import { Transaction } from "../../shared/enums";

export class RequestResultDto {

    @ApiProperty()
    readonly _id?: string;

    @ApiProperty()
    readonly rawResponse: string;

    @ApiProperty()
    readonly approveCode: string;

    @ApiProperty()
    readonly cardNumbers: string;

    @ApiProperty()
    readonly accountType: string;

    @ApiProperty()
    readonly franchise: string;

    @ApiProperty()
    readonly amount: number;

    @ApiProperty()
    readonly taxes: number;

    @ApiProperty()
    readonly impoConsumo: number;

    @ApiProperty()
    readonly receiptNumber: string;

    @ApiProperty()
    readonly paymentID: string;

    @ApiProperty()
    readonly installments: string;

    @ApiProperty()
    readonly payeeTransactionId: string;

    @ApiProperty()
    readonly terminalNumber: string;

    @ApiProperty()
    readonly tenantNumber: string;

    @ApiProperty()
    readonly date: string;

    @ApiProperty()
    readonly hour: string;

    @ApiProperty()
    readonly voucherNumber: string;

    @ApiProperty()
    readonly operationType: string;
}

export class PaymentRequestDto {

    @ApiProperty()
    readonly _id?: string;

    @ApiProperty()
    readonly terminal: string;

    @ApiProperty()
    readonly paymentUrl: string;

    @ApiProperty()
    readonly operation: string;

    @ApiProperty()
    readonly amount: number;

    @ApiProperty()
    readonly tip: number;

    @ApiProperty()
    readonly tax: number;

    @ApiProperty()
    readonly invoice: string;

    @ApiProperty()
    readonly taxReturnBase: number;

    @ApiProperty()
    readonly taxRateBase: number;

    @ApiProperty()
    readonly cashierID: string;

    @ApiProperty()
    readonly impoConsumo: number;

    @ApiProperty()
    readonly impoConsumoBase: number;

    @ApiProperty()
    readonly receipt: string;

    @ApiProperty({ enum: Transaction.STATUS })
    readonly status: Transaction.STATUS;

    @ApiProperty()
    readonly response: RequestResultDto;

    @ApiProperty()
    readonly responseAttempts: number;

    @ApiProperty()
    readonly createdAt: string;

    @ApiProperty()
    readonly updatedAt: string;
}
