import { ApiProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";
import { IsDefined, IsNotEmpty, IsNumber, IsOptional, IsString, ValidateNested } from "class-validator";
import { Transaction } from "../../shared/enums";

export class Response {

    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    readonly idPaymentRequest: string;

    @Expose()
    @IsString()
    @IsOptional()
    @ApiProperty()
    readonly approveCode?: string;

    @Expose()
    @IsString()
    @IsOptional()
    @ApiProperty()
    readonly date?: string;

    @Expose()
    @IsString()
    @IsOptional()
    @ApiProperty()
    readonly franchise?: string;
    @Expose()
    @IsString()
    @IsOptional()
    @ApiProperty()
    readonly messageResponse?: string;

    @Expose()
    @IsString()
    @IsOptional()
    @ApiProperty()
    readonly confirmNumber?: string;

    @Expose()
    @IsString()
    @IsOptional()
    @ApiProperty()
    readonly numberInstallments?: string;

    @Expose()
    @IsString()
    @IsOptional()
    @ApiProperty()
    readonly receiptNumber?: string;

    @Expose()
    @IsString()
    @IsOptional()
    @ApiProperty()
    readonly accountType?: string;

    @Expose()
    @IsString()
    @IsOptional()
    @ApiProperty()
    readonly operationType?: string;

    @Expose()
    @IsString()
    @IsOptional()
    @ApiProperty()
    readonly cardNumbers?: string;

    @Expose()
    @IsOptional()
    @IsNumber()
    @ApiProperty()
    readonly amount?: number;

    @Expose()
    @IsOptional()
    @IsNumber()
    @ApiProperty()
    readonly taxes?: number;
}

export class TransactionResponseRequestDto {

    @ApiProperty()
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly status: Transaction.STATUS;

    @ApiProperty()
    @Expose()
    @IsDefined()
    @ValidateNested()
    readonly response: Response;
}