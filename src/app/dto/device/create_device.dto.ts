import { ApiProperty } from "@nestjs/swagger";
import { Expose, Type } from "class-transformer";
import { IsDateString, IsDefined, IsMongoId, IsNotEmpty, IsOptional, IsString, ValidateNested } from "class-validator";
import { PaymentMethodDto } from "./payment-methods.dto";

export class CreateDeviceDto {

    @Expose()
    @IsMongoId()
    @IsDefined()
    @IsNotEmpty()
    @ApiProperty()
    readonly _sellerID: string;

    @Expose()
    @IsString()
    @IsDefined()
    @IsNotEmpty()
    @ApiProperty()
    readonly deviceID: string;

    @Expose()
    @IsString()
    @IsDefined()
    @IsNotEmpty()
    @ApiProperty()
    readonly deviceName: string;

    @Expose()
    @IsString()
    @IsDefined()
    @IsNotEmpty()
    @ApiProperty()
    readonly deviceModel: string;

    @Expose()
    @IsString()
    @IsDefined()
    @IsNotEmpty()
    @ApiProperty()
    readonly deviceLabel: string;

    @Expose()
    @IsString()
    @IsDefined()
    @IsNotEmpty()
    @ApiProperty()
    readonly deviceType: string;

    @Expose()
    @IsString()
    @IsDefined()
    @IsNotEmpty()
    @ApiProperty()
    readonly os: string;

    @Expose()
    @IsString()
    @IsDefined()
    @IsNotEmpty()
    @ApiProperty()
    readonly osVersion: string;

    @Expose()
    @Type(() => PaymentMethodDto)
    @IsDefined()
    @ValidateNested()
    @ApiProperty()
    readonly paymentMethods: PaymentMethodDto;

    @Expose()
    @IsOptional()
    @IsDateString()
    @ApiProperty()
    readonly createdAt?: string;

    @Expose()
    @IsOptional()
    @IsDateString()
    @ApiProperty()
    updatedAt?: string;
}
