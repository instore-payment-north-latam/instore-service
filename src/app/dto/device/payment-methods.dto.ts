import { ApiProperty } from "@nestjs/swagger";
import { Expose, Type } from "class-transformer";
import { ValidateNested } from "class-validator";
import { MercadoPagoDto } from "./mercado-pago.dto";
import { RedebanDto } from "./redeban.dto";

export class PaymentMethodDto {
  @Expose()
  @Type(() => RedebanDto)
  @ValidateNested()
  @ApiProperty()
  readonly redeban: RedebanDto;

  @Expose()
  @Type(() => MercadoPagoDto)
  @ValidateNested()
  @ApiProperty()
  readonly mercadopago: MercadoPagoDto;
}