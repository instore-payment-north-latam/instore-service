import { ApiProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";
import { IsString } from "class-validator";

export class MercadoPagoDto {
  @Expose()
  @IsString()
  @ApiProperty()
  readonly client_id: string;

  @Expose()
  @IsString()
  @ApiProperty()
  readonly client_secret: string;
}
