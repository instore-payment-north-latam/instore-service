import { ApiProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";
import { IsString } from "class-validator";

export class RedebanDto {
  @Expose()
  @IsString()
  @ApiProperty()
  readonly model: string;

  @Expose()
  @IsString()
  @ApiProperty()
  readonly terminal: string;

  @Expose()
  @IsString()
  @ApiProperty()
  readonly cashierID: string;

  @Expose()
  @IsString()
  @ApiProperty()
  readonly url: string;
}