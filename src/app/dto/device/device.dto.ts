import { ApiProperty } from "@nestjs/swagger";
import { Expose, Type } from "class-transformer";
import { IsString, ValidateNested } from "class-validator";
import { PaymentMethodDto } from "./payment-methods.dto";
import { ObjectId } from 'mongodb';
export const objectToString = (id: ObjectId) => new ObjectId(id).toHexString();

export class DeviceDto {

  @Expose()
  @IsString()
  @ApiProperty()
  readonly _id: string;

  @Expose()
  @IsString()
  @ApiProperty()
  readonly _sellerID: string;

  @Expose()
  @IsString()
  @ApiProperty()
  readonly deviceID: string;

  /**
   * @todo Validate with Adrian, because it is not registering in mongo
   */
  @Expose()
  @IsString()
  @ApiProperty()
  readonly deviceName: string;

  @Expose()
  @IsString()
  @ApiProperty()
  readonly deviceLabel: string;

  @Expose()
  @IsString()
  @ApiProperty()
  readonly deviceModel: string;

  @Expose()
  @IsString()
  @ApiProperty()
  readonly deviceType: string;

  @Expose()
  @IsString()
  @ApiProperty()
  readonly os: string;

  @Expose()
  @IsString()
  @ApiProperty()
  readonly osVersion: string;

  @Expose()
  @Type(() => PaymentMethodDto)
  @ValidateNested()
  @ApiProperty()
  readonly paymentMethods: PaymentMethodDto;

  @Expose()
  @IsString()
  @ApiProperty()
  readonly createdAt: string;

  @Expose()
  @IsString()
  @ApiProperty()
  readonly updatedAt: string;
}
