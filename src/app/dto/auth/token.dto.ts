import { ApiProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";
import { IsDefined, IsString } from "class-validator";
import { IsJWT } from "class-validator";

export class TokenDto {
    @ApiProperty({ description: "String con el formato de JWT" })
    @Expose()
    @IsString()
    @IsDefined()
    @IsJWT()
    readonly token: string;
}