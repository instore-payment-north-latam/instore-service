import { ApiProperty } from "@nestjs/swagger";
import { Expose } from "class-transformer";
import { IsDefined, IsNotEmpty, IsString } from "class-validator";

export class CredentialsDto {

    @ApiProperty({ description: "Credenciales cifradas con llave publica" })
    @Expose()
    @IsDefined()
    @IsNotEmpty()
    @IsString()
    readonly value: string;

}