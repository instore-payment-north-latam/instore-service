import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from "@nestjs/mongoose";
import { Transaction } from '../interfaces/transaction.interface';
import { TransactionDto } from "../dto/transaction/transaction.dto";
import { TransactionRequestDto } from "../dto/transaction/transaction-request.dto";
import * as moment from 'moment';
import { SettingService } from "../settings/settings.service";
import { DeviceService } from "../device/device.service";
import { SellerService } from "../seller/seller.service";
import { Transaction as TransactionEnum } from '../shared/enums';

@Injectable()
export class TransactionService {
    constructor(
        @InjectModel('Transaction') private readonly transactionModel: Model<Transaction>,
        private settingsService: SettingService,
        private deviceService: DeviceService,
        private sellerService: SellerService
    ) { }

    async addTransaction(transactionRequestDto: TransactionRequestDto): Promise<Transaction> {
        const storeInfo = await this.getStoreInfo(transactionRequestDto.deviceId);

        const createTransactionDTO: TransactionDto = {
            orderId: transactionRequestDto.orderId,
            paymentId: transactionRequestDto.paymentId,
            transactionId: transactionRequestDto.transactionId || "",
            paymentRequestId: "",
            sellerName: storeInfo.storeName,
            sellerWhiteLabel: storeInfo.seller.seller,
            deviceId: transactionRequestDto.deviceId,
            /**
             * @todo Validar con James el campo opcional en mongoDB
             * ya que los dispositivos a920 no tiene terminal "paymentDeviceID=device.terminal"
             */
            paymentDeviceId: storeInfo.paymentDeviceID || "",
            paymentType: transactionRequestDto.paymentType,
            payerEmail: transactionRequestDto.payerEmail,
            payerIdentification: transactionRequestDto.payerIdentification,
            amount: transactionRequestDto.amount,
            taxRate: storeInfo.taxRate,
            status: TransactionEnum.STATUS.PENDING,
            debugMode: transactionRequestDto.debugMode,
            createdAt: moment().toISOString(),
            updatedAt: moment().toISOString(),
            isConfirmed: false
        };
        const newTransaction = await new this.transactionModel(createTransactionDTO);
        return newTransaction.save();
    }

    async updateTransactionResponse(paymentId: string, status: TransactionEnum.STATUS): Promise<void> {
        /**
         * Se actualiza la transaccion con un status diferente
         */
        const transaction = await this.getTransactionsByRef(paymentId);
        transaction.status = status;
        await this.updateTransaction(transaction._id, transaction);
    }

    async getTransaction(id): Promise<Transaction> {
        const transaction = await this.transactionModel
            .findById(id)
            .exec();
        return transaction;
    }

    async getTransactions(): Promise<Transaction[]> {
        const transactions = await this.transactionModel
            .find()
            .exec()
        return transactions;
    }

    async getTransactionsByDevice(id): Promise<Transaction[]> {
        const transactions = await this.transactionModel
            .find({
                deviceId: id
            })
            .exec()
        return transactions
    }

    async getTransactionsByRef(id): Promise<Transaction> {
        const transaction = await this.transactionModel
            .findOne({
                paymentId: id
            })
            .exec();
        return transaction;
    }

    async getTransactionsById(transactionId: string): Promise<Transaction> {
        const transaction = await this.transactionModel
            .findOne({
                transactionId
            })
            .exec();
        return transaction;
    }

    async updateTransaction(id: string, createTransactionDTO: TransactionDto): Promise<Transaction> {
        const update = await this.transactionModel
            .findByIdAndUpdate(id, createTransactionDTO, { new: true })
        return update;
    }

    async deleteTransaction(id): Promise<any> {
        const deleteTransaction = await this.transactionModel
            .findByIdAndRemove(id);
        return deleteTransaction;
    }

    async getStoreInfo(deviceId): Promise<any> {
        const device = await this.deviceService.getDevice(deviceId);

        this.deviceService.validateDevice(device);

        const seller = await this.sellerService.getSellerId(device._sellerID);
        const settings = await this.settingsService.getSettingsById(seller._configID);

        return {
            seller,
            storeName: settings.seller,
            paymentDeviceID: device.paymentMethods.redeban.terminal,
            taxRate: settings.taxRate / 100
        };
    }
}
