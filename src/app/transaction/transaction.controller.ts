import { Controller, Get, Res, HttpStatus, Param, NotFoundException, Post, Body, Put, Delete, UseGuards, Patch, UnprocessableEntityException } from '@nestjs/common';
import { TransactionService } from "./transaction.service";
import { TransactionDto } from "../dto/transaction/transaction.dto";
import { TransactionRequestDto } from "../dto/transaction/transaction-request.dto";
import { ValidateObjectId } from "../shared/pipes/validate-object-id.pipes";
import { PaymentRequestService } from "../payment-request/payment-request.service";
import { TransactionResponseRequestDto } from '../dto/transaction/transaction-response-request.dto';
import { AuthGuard } from '@nestjs/passport';
import { Transaction as enumTransaction } from '../shared/enums';

import { ApiBearerAuth, ApiParam, ApiResponse, ApiTags, ApiUnauthorizedResponse } from "@nestjs/swagger";
import { PaymentRequestDto } from '../dto/transaction/create-payment-request.dto';
import { ResponseTransactionDto } from '../dto/transaction/response-transaction.dto';
import { NotFoundExceptionDto } from '../dto/shared/not-found-exception.dto';
import { UnauthorizedExceptionDto } from '../dto/shared/unauthorized-exception.dto';
import { Transaction } from '../interfaces/transaction.interface';

/**
 * @todo Add auth
 */
// @UseGuards(AuthGuard())
@ApiTags('Transactions')
@ApiBearerAuth()
@ApiUnauthorizedResponse({ description: "Unauthorized", type: UnauthorizedExceptionDto })
@Controller('api/transactions')
export class TransactionController {
    constructor(
        private transactionService: TransactionService,
        private paymentRequestService: PaymentRequestService
    ) { }

    @Post('/')
    @ApiResponse({
        status: HttpStatus.OK,
        description: "Processed",
        type: ResponseTransactionDto
    })
    @ApiResponse({
        status: HttpStatus.CREATED,
        description: "Transaction created successfully",
        type: PaymentRequestDto
    })
    @ApiResponse({
        status: HttpStatus.UNPROCESSABLE_ENTITY,
        description: "A001",
        type: UnprocessableEntityException
    })
    async addTransaction(@Res() res, @Body() transactionRequestDto: TransactionRequestDto) {
        let transaction: Transaction = null;

        transaction = await this.transactionService.getTransactionsById(transactionRequestDto.transactionId);
        if (!transaction) {
            transaction = await this.transactionService.getTransactionsByRef(transactionRequestDto.paymentId);
        }

        if (!transaction) {
            transaction = await this.transactionService.addTransaction(transactionRequestDto);
        } else {
            const payed = await this.paymentRequestService.checkIfTransactionIsAlreadyPayed(transaction.orderId);
            if (payed) {
                throw new UnprocessableEntityException('A001');
            }
            
            // transaction = transaction.toObject<TransactionDto | any>();
            // if (payed) {
            //     const paymentRequest = await this.paymentRequestService.getService(transaction.paymentRequestId);

            //     return res.status(HttpStatus.OK).json({
            //         message: 'Processed',
            //         data: {
            //             ...transaction,
            //             paymentResponse: paymentRequest
            //         }
            //     });
            // }
        }

        /**
         * Crea payment request
         */
        const paymentRequest = await this.paymentRequestService.create(transaction, 'pay');

        /**
         * Actualiza la transaccion con el paymentRequestId recien creado
         */
        transaction.paymentRequestId = paymentRequest._id;
        transaction.status = enumTransaction.STATUS.PENDING;
        await this.transactionService.updateTransaction(transaction._id, transaction);

        return res.status(HttpStatus.CREATED).json({
            message: 'Transaction created successfully',
            data: paymentRequest.toObject<PaymentRequestDto>()
        });
    }

    @Get('/:idTransaction')
    @ApiParam({ name: "idTransaction" })
    @ApiResponse({
        status: HttpStatus.OK,
        description: "Transaction retrieved",
        type: TransactionDto
    })
    async getTransaction(@Res() res, @Param('idTransaction', new ValidateObjectId()) idTransaction) {
        const transaction = await this.transactionService.getTransaction(idTransaction);

        return res.status(HttpStatus.OK).json({
            message: 'Transaction retrieved',
            data: transaction.toObject<TransactionDto>()
        })
    }

    @Get('/payment/:idPaymentRequest')
    @ApiParam({ name: "idPaymentRequest" })
    @ApiResponse({
        status: HttpStatus.OK,
        description: "Transaction retrieved",
        type: PaymentRequestDto
    })
    @ApiResponse({
        status: HttpStatus.NOT_FOUND,
        description: "Payment request not found",
        type: NotFoundExceptionDto
    })
    async getPaymentRequest(@Res() res, @Param('idPaymentRequest', new ValidateObjectId()) idPaymentRequest) {
        const paymentRequest = await this.paymentRequestService.getService(idPaymentRequest);

        if (!paymentRequest) {
            throw new NotFoundException("Payment request not found");
        }

        return res.status(HttpStatus.OK).json({
            message: "Payment retrieved",
            data: paymentRequest.toObject<PaymentRequestDto>()
        })
    }

    @Get('/')
    @ApiResponse({
        status: HttpStatus.OK,
        description: "Transactions retrieved",
        type: TransactionDto,
        isArray: true
    })
    @ApiResponse({
        status: HttpStatus.NOT_FOUND,
        description: "No transactions found",
        type: NotFoundExceptionDto
    })
    async getTransactions(@Res() res) {
        const transactions = await this.transactionService.getTransactions();
        if (transactions.length == 0) {
            throw new NotFoundException('No transactions found');
        }
        return res.status(HttpStatus.OK).json({
            message: 'Transactions retrieved',
            data: transactions
        })
    }

    @Get('/device/:idDevice')
    @ApiParam({ name: "idDevice" })
    @ApiResponse({
        status: HttpStatus.OK,
        description: "Transactions for device -{idDevice}- retrieved",
        type: TransactionDto,
        isArray: true
    })
    @ApiResponse({
        status: HttpStatus.NOT_FOUND,
        description: "No transactions found",
        type: NotFoundExceptionDto
    })
    async getTransactionsByDevice(@Res() res, @Param('idDevice',) idDevice) {
        const transactions = await this.transactionService.getTransactionsByDevice(idDevice)
        if (transactions.length == 0) {
            throw new NotFoundException('No transactions found for this device');
        }
        return res.status(HttpStatus.OK).json({
            message: 'Transactions for device -' + idDevice + '- retrieved',
            data: transactions
        })
    }

    @Put('/payment/alreadypaid/:idTransaction')
    @ApiParam({ name: "idTransaction" })
    @ApiResponse({
        status: HttpStatus.OK,
        description: "Transaction updated",
        type: Boolean
    })
    @ApiResponse({
        status: HttpStatus.NOT_FOUND,
        description: "Transaction not found",
        type: NotFoundExceptionDto
    })
    async updatePaid(
        @Res() res,
        @Param('idTransaction', new ValidateObjectId()) idTransaction
    ) {

        const transaction = await this.transactionService.getTransaction(idTransaction);

        if (!transaction) {
            throw new NotFoundException('Transaction not found');
        }

        transaction.isConfirmed = true;
        await this.transactionService.updateTransaction(idTransaction, transaction.toObject<TransactionDto>());

        return res.status(HttpStatus.OK).json({
            message: 'Transaction updated',
            data: true
        });
    }

    @Put('/:idTransaction')
    @ApiParam({ name: "idTransaction" })
    @ApiResponse({
        status: HttpStatus.OK,
        description: "Transaction updated",
        type: TransactionDto
    })
    @ApiResponse({
        status: HttpStatus.NOT_FOUND,
        description: "Transaction not found",
        type: NotFoundExceptionDto
    })
    async updateTransaction(
        @Res() res,
        @Body() createTransactionDTO: TransactionDto,
        @Param('idTransaction', new ValidateObjectId()) idTransaction
    ) {
        const update = await this.transactionService.updateTransaction(idTransaction, createTransactionDTO);

        if (!update) {
            throw new NotFoundException('Transaction not found')
        }
        return res.status(HttpStatus.OK).json({
            message: 'Transaction updated',
            data: update
        });
    }

    /**
     * Endpoint para dispositivos a920
     */
    @Patch('/')
    @ApiResponse({
        status: HttpStatus.OK,
        description: "Transaction updated"
    })
    async updateTransactions(@Res() res, @Body() body: TransactionResponseRequestDto) {

        const paymentRequest = await this.paymentRequestService.updatePaymentRequest(body);
        /**
         * Cuando se crea el payment request (api/transaction/)
         * Se asigna transaction.paymentId a paymentRequest.receipt
         */
        await this.transactionService.updateTransactionResponse(paymentRequest.receipt, body.status);

        return res.status(HttpStatus.OK).json({
            message: 'Transaction updated'
        });
    }

    @Delete('/:idTransaction')
    @ApiParam({ name: "idTransaction" })
    @ApiResponse({
        status: HttpStatus.NOT_FOUND,
        description: "Transaction not found",
        type: NotFoundExceptionDto
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: "Transaction deleted",
        type: TransactionDto
    })
    async deleteTransaction(@Res() res, @Param('idTransaction', new ValidateObjectId()) idTransaction) {
        const deleteTransaction = await this.transactionService.deleteTransaction(idTransaction);
        if (!deleteTransaction) {
            throw new NotFoundException('Transaction not found')
        }
        return res.status(HttpStatus.OK).json({
            message: 'Transaction deleted',
            data: deleteTransaction
        });
    }
}
