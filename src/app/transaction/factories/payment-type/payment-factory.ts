import { Injectable } from '@nestjs/common';
import { WebServiceRedebanPay } from './web-service-redeban-pay';
import { PaymentRequest } from '../../../interfaces/payment_request.interface';
import { Transaction } from '../../../shared/enums';



@Injectable()
export class PaymentFactory {
    constructor(
        private webServiceRedebanPay: WebServiceRedebanPay
    ) { }

    async create(paymentType: Transaction.PAYMENT_TYPE, payment: PaymentRequest): Promise<void> {

        switch (paymentType) {

            case Transaction.PAYMENT_TYPE.WEB_SERVICE_REDEBAN:
                await this.webServiceRedebanPay.create(payment);
                break;
            /**
             * @description example
             * case Transaction.PAYMENT_TYPE.DEEP_LINKING_REDEBAN:
             *     await this.deepLinkingRedeban.create(payment);
             */
        }

    }
}