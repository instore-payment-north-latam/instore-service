import { Injectable, GoneException } from '@nestjs/common';
import { Queue } from 'bull';
import { InjectQueue } from "@nestjs/bull";
import * as Soap from 'soap';
import * as moment from "moment";
import { PaymentRequest } from '../../../interfaces/payment_request.interface';
import { RedebanQueue } from './../../../payment-request/queues/redeban-queue.interface';

@Injectable()
export class WebServiceRedebanPay {
    constructor(
        @InjectQueue('redeban') private redebanQueue: Queue<RedebanQueue>
    ) { }

    async create(payment: PaymentRequest): Promise<void> {
        const payloadRequest = ''.concat(
            this.getOperationType(payment.operation), ',',
            payment.amount.toString(), ',',
            payment.tip.toString(), ',',
            payment.tax.toString(), ',',
            payment.invoice, ',',
            payment.taxReturnBase.toString(), ',',
            payment.cashierID, ',',
            payment.impoConsumo.toString(), ',',
            payment.taxRateBase.toString(), ',',
            payment.impoConsumoBase.toString(), ',',
            payment.receipt
        );


        const url = ((payment.paymentUrl).includes('?WSDL')) ? payment.paymentUrl : payment.paymentUrl + '?WSDL';
        const args = {
            Terminal: payment.terminal,
            Data: payloadRequest,
        };

        const client = await Soap.createClientAsync(url);
        try {
            await client.EscribirSolicitudPCAsync(args);

            await this.redebanQueue.add(
                {
                    request: payloadRequest,
                    /**
                     * Id del payment request
                     */
                    paymentID: payment._id,
                    terminal: payment.terminal,
                    url,
                    attempts: 1,
                    date: moment().toISOString()
                }, {
                jobId: payment._id,
                delay: 20000,
                removeOnFail: true,
                removeOnComplete: true,
                repeat: {
                    every: 10000,
                    limit: 15
                }
            });
        } catch (e) {
            throw new GoneException('Chip and Pin communication failed, e00:' + e);
        }
    }

    private getOperationType(type) {
        switch (type) {
            case 'refund':
                return '1';
            default:
                return '0';
        }
    }

}