import { HttpModule, Module } from '@nestjs/common';
import { TransactionService } from './transaction.service';
import { TransactionController } from './transaction.controller';
import { MongooseModule } from "@nestjs/mongoose";
import { TransactionSchema } from "../schemas/transaction.schema";
import { PaymentRequestService } from "../payment-request/payment-request.service";
import { PaymentRequestSchema } from "../schemas/payment_request.schema";
import { SettingModule } from "../settings/settings.module";
import { BullModule } from "@nestjs/bull";
import { RedebanQueueConsumer } from "../payment-request/queues/redeban-queue.consumer";
import { PaymentFactory } from "./factories/payment-type/payment-factory";
import { WebServiceRedebanPay } from "./factories/payment-type/web-service-redeban-pay";
import { AuthModule } from '../auth/auth.module';

@Module({
    imports: [
        AuthModule,
        HttpModule.register({}),
        MongooseModule.forFeature([
            { name: 'Transaction', schema: TransactionSchema },
            { name: 'PaymentRequest', schema: PaymentRequestSchema }
        ]),
        BullModule.registerQueue({
            name: 'redeban',
            redis: {
                host: 'localhost',
                port: 6379,
            },
            defaultJobOptions: {
                removeOnComplete: true,
                removeOnFail: true,
            }
        }),
        SettingModule
    ],
    providers: [
        TransactionService,
        PaymentRequestService,
        RedebanQueueConsumer,
        PaymentFactory,
        WebServiceRedebanPay
    ],
    controllers: [TransactionController]
})
export class TransactionModule { }

