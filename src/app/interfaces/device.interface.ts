import { Document } from 'mongoose'

export interface Device extends Document {
    readonly _sellerID: string;
    readonly deviceID: string;
    readonly deviceName: string;
    readonly deviceLabel: string;
    readonly deviceModel: string;
    readonly deviceType: string;
    readonly os: string;
    readonly osVersion: string;
    readonly paymentMethods: PaymentMethodsInterface;
    readonly createdAt: string;
    readonly updatedAt: string;
}


/******************* Interfaz medios de pago *******************/

declare interface PaymentMethodsInterface {
    readonly redeban: RedebanInterface;
    readonly mercadopago: MercadoPagoInterface;
}

declare interface RedebanInterface {
    readonly model: string;
    readonly terminal: string;
    readonly cashierID: string;
    readonly url: string;
}

declare interface MercadoPagoInterface {
    readonly client_id: string;
    readonly client_secret: string;
}
