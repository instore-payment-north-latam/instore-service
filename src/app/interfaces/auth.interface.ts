import { Document } from 'mongoose';

export interface Auth extends Document {
    readonly apiKey: string;
    readonly apiToken: string;
}
