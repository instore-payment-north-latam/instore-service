import { Document, Model } from 'mongoose'
import { PaginateMethod } from '../utils/mongoose-paginate-v2.util';

export interface Seller extends Document {
    readonly _configID: string;
    readonly seller: string;
    readonly sellerName: string;
    readonly pickupPointID: string;
    readonly address: AddressInterface;
    readonly paymentMethods: PaymentMethodsInterface;
    readonly createdAt: string;
    readonly updatedAt: string;
}

export interface ModelSeller<T> extends Model<T> {
    paginate: PaginateMethod<T>;
}


/******************* Interfaz medios de pago *******************/

declare interface PaymentMethodsInterface {
    readonly redeban: RedebanInterface;
    readonly mercadopago: MercadoPagoInterface;
}

declare interface RedebanInterface {
    readonly model: string;
    readonly terminal: string;
    readonly url: string;
}

declare interface MercadoPagoInterface {
    readonly client_id: string;
    readonly client_secret: string;
}

/******************* Interfaz dirección *******************/

declare interface AddressInterface {
    readonly country: string;
    readonly state: string;
    readonly city: string;
    readonly complement: string;
    readonly location: LocationInterface;
    readonly neighborhood: string;
    readonly number: string;
    readonly postalCode: string;
    readonly street: string;
}

declare interface LocationInterface {
    readonly latitude: number;
    readonly longitude: number;
}
