import { Document } from 'mongoose';
import { Transaction } from '../shared/enums';

export interface PaymentRequest extends Document {
    readonly terminal: string,
    readonly paymentUrl: string,
    readonly operation: string,
    readonly amount: number,
    readonly tip: number,
    readonly tax: number,
    readonly invoice: string,
    readonly taxReturnBase: number,
    readonly taxRateBase: number,
    readonly cashierID: string,
    readonly impoConsumo: number,
    readonly impoConsumoBase: number,
    readonly receipt: string,
    readonly status: Transaction.STATUS,
    response: string,
    readonly responseAttempts: number,
    readonly createdAt: string,
    readonly updatedAt: string,
}