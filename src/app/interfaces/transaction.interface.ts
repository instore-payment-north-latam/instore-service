import { Document } from 'mongoose';
import { Transaction } from '../shared/enums';

export interface Transaction extends Document {
    readonly orderId: string;
    readonly paymentId: string;
    readonly transactionId: string;
    paymentRequestId: string;
    readonly sellerName: string;
    readonly sellerWhiteLabel: string;
    readonly deviceId: string;
    readonly paymentDeviceId: string,
    readonly paymentType: Transaction.PAYMENT_TYPE;
    readonly payerEmail: string;
    readonly payerIdentification: string;
    readonly amount: number;
    readonly taxRate: number;
    status: Transaction.STATUS;
    readonly debugMode: boolean;
    readonly createdAt: string;
    readonly updatedAt: string;
    isConfirmed: boolean;
}