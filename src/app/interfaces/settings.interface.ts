import { Document } from 'mongoose'

export interface Setting extends Document {
    readonly seller: string;
    readonly active: string;
    readonly sellerName: string;
    readonly sellerTaxID: string;
    readonly country: string;
    readonly taxRate: number;
    readonly paymentMethods: PaymentMethodsInterface;
    readonly createdAt: string;
    readonly updatedAt: string;
}

declare interface RedebanInterface {
    readonly active: boolean;
    readonly url: string;
}

declare interface CredibancoInterface {
    readonly active: boolean;
    readonly url: string;
}

declare interface MercadoPagoInterface {
    readonly active: boolean;
    readonly client_id: string;
    readonly client_secret: string;
}

declare interface BancolombiaInterface {
    readonly active: boolean;
}

declare interface NequiInterface {
    readonly active: boolean;
}

declare interface DaviPlataInterface {
    readonly active: boolean;
}

declare interface PaymentMethodsInterface {
    readonly redeban: RedebanInterface;
    readonly credibanco: CredibancoInterface;
    readonly mercadopago: MercadoPagoInterface;
    readonly bancolombia: BancolombiaInterface;
    readonly daviplata: DaviPlataInterface;
}
