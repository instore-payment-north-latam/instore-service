export interface JwtPayload {
    apiKey: string;
    apiToken: string;
}