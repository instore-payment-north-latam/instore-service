import * as mongoose from 'mongoose';

export const AuthSchema = new mongoose.Schema({
    apiKey: {
        type: String,
        required: true
    },
    apiToken: {
        type: String,
        required: true
    }
});
