import * as mongoose from 'mongoose';
import * as moment from 'moment';

export const SettingsSchema = new mongoose.Schema({
    seller: {
        type: String,
        unique: true,
        required: true
    },
    active: Boolean,
    sellerName: {
        type: String,
        required: true
    },
    sellerTaxID: {
        type: String,
        required: true
    },
    country: {
        type: String,
        required: true
    },
    taxRate: {
        type: Number,
        default: 19,
    },
    paymentMethods: {
        redeban: {
            active: Boolean,
            url: String
        },
        credibanco: {
            active: Boolean,
            url: String
        },
        mercadopago: {
            active: Boolean,
            client_id: String,
            client_secret: String
        },
        bancolombia: {
            active: Boolean
        },
        nequi: {
            active: Boolean
        },
        daviplata: {
            active: Boolean
        }
    },
    createdAt: {
        type: Date,
        default: moment().toISOString()
    },
    updatedAt: {
        type: Date,
        default: moment().toISOString()
    }
});
