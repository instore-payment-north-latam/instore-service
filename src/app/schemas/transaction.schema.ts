/* instore-transactions-service/src/app/schemas/transaction.schema.ts */
import * as mongoose from 'mongoose';
import * as moment from 'moment';

export const TransactionSchema = new mongoose.Schema({
    orderId: {
        type: String,
        required: true
    },
    paymentId: {
        type: String,
        required: true,
        unique: true
    },
    transactionId: {
        type: String,
        required: true,
        unique: true
    },
    paymentRequestId: {
        type: String
    },
    sellerName: {
        type: String,
        required: true
    },
    sellerWhiteLabel: {
        type: String,
        required: true
    },
    deviceId: {
        type: String,
        required: true
    },
    paymentDeviceId: {
        type: String
    },
    paymentType: {
        type: String,
        required: true
    },
    payerEmail: {
        type: String,
        required: true
    },
    payerIdentification: {
        type: String,
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
    taxRate: {
        type: Number,
        required: true
    },
    status: {
        type: String,
        default: 'pending'
    },
    debugMode: {
        type: Boolean,
        required: true
    },
    createdAt: {
        type: Date,
        default: moment().toISOString()
    },
    updatedAt: {
        type: Date,
        default: moment().toISOString()
    },
    isConfirmed: {
        type: Boolean,
        default: false
    }
});
