import * as mongoose from 'mongoose';
import * as moment from "moment";

export const DevicesSchema = new mongoose.Schema({
    _sellerID: mongoose.Schema.Types.ObjectId,
    deviceID: String,
    deviceLabel: String,
    deviceType: String,
    deviceModel: String,
    os: String,
    osVersion: String,
    paymentMethods: {
        redeban: {
            model: String,
            terminal: String,
            cashierID: {
                type: String,
                default: "01"
            },
            url: String,
        },
        mercadopago: {
            client_id: String,
            client_secret: String,
        }
    },
    createdAt: {
        type: Date,
        default: moment().toISOString()
    },
    updatedAt: {
        type: Date,
        default: moment().toISOString()
    }
})
