import * as mongoose from 'mongoose';
import * as moment from 'moment';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export const SellersSchema = new mongoose.Schema({
    _configID: mongoose.Schema.Types.ObjectId,
    seller: String,
    sellerName: String,
    pickupPointID: String,
    address: {
        country: String,
        state: String,
        city: String,
        complement: String,
        location: {
            latitude: Number,
            longitude: Number
        },
        neighborhood: String,
        number: String,
        postalCode: String,
        street: String,
    },
    paymentMethods: {
        redeban: {
            terminal: String,
            model: String,
            url: String,
        },
        mercadopago: {
            client_id: String,
            client_secret: String
        }
    },
    createdAt: {
        type: Date,
        default: moment().toISOString()
    },
    updatedAt: {
        type: Date,
        default: moment().toISOString()
    }
})
    .index({ _configID: 1, seller: 1 }, { unique: true })
    .plugin(mongoosePaginate);



