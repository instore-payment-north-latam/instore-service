import * as mongoose from 'mongoose';
import * as moment from 'moment';

const PaymentResultSchema = new mongoose.Schema({
    rawResponse: String,
    approveCode: String,
    cardNumbers: String,
    accountType: String,
    franchise: String,
    amount: Number,
    taxes: Number,
    impoConsumo: Number,
    receiptNumber: String,
    paymentID: String,
    installments: String,
    payeeTransactionId: String,
    terminalNumber: String,
    tenantNumber: String,
    date: String,
    hour: String,
    voucherNumber: String,
    operationType: String,
});

export const PaymentRequestSchema = new mongoose.Schema({
    terminal: {
        type: String
    },
    paymentUrl: {
        type: String,
    },
    operation: {
        type: String,
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
    tip: {
        type: Number,
        required: true
    },
    tax: {
        type: Number,
        required: true
    },
    invoice: {
        type: String,
        required: true
    },
    taxReturnBase: {
        type: Number,
        required: true
    },
    taxRateBase: {
        type: Number,
        required: true
    },
    cashierID: {
        type: String,
        required: true
    },
    impoConsumo: {
        type: Number,
        required: true
    },
    impoConsumoBase: {
        type: Number,
        required: true
    },
    receipt: {
        type: String,
        required: true
    },
    status: String,
    response: PaymentResultSchema,
    responseAttempts: Number,
    createdAt: {
        type: Date,
        default: moment().toISOString()
    },
    updatedAt: {
        type: Date,
        default: moment().toISOString()
    }
});
