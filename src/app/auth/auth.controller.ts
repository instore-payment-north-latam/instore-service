import { Body, Controller, HttpStatus, Post, Put, Res, UnauthorizedException } from "@nestjs/common"
import { ApiResponse, ApiTags } from "@nestjs/swagger";
import { CredentialsDto } from "../dto/auth/credentials.dto";
import { TokenDto } from "../dto/auth/token.dto";
import { UnauthorizedExceptionDto } from "../dto/shared/unauthorized-exception.dto";
import { AuthService } from "./auth.service";


@ApiTags('Auth')
@Controller('api/auth')
export class AuthController {
    constructor(private authService: AuthService) { }

    /**
     * @param res 
     * @param credentials 
     */
    @Post('/token')
    @ApiResponse({ status: HttpStatus.OK, type: String, description: "success" })
    @ApiResponse({ status: HttpStatus.UNAUTHORIZED, type: UnauthorizedExceptionDto, description: "Access token invalid" })
    async getSeller(@Res() res, @Body() credentials: CredentialsDto) {

        const token = await this.authService.createTokenJWT(credentials);

        return res.status(HttpStatus.OK).json({
            message: 'success',
            data: token
        })
    }

    @Put('/token')
    @ApiResponse({ status: HttpStatus.OK, type: String, description: "success" })
    @ApiResponse({ status: HttpStatus.UNAUTHORIZED, type: UnauthorizedExceptionDto, description: "Access token invalid" })
    async updateToken(@Res() res, @Body() createSellerDTO: TokenDto) {
        const token = await this.authService.refreshToken(createSellerDTO.token);

        return res.status(HttpStatus.OK).json({
            message: 'success',
            data: token
        })
    }
}
