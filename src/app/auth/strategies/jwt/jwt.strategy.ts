import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import { CredentialsDto } from "src/app/dto/auth/credentials.dto";
import { AuthService } from "../../auth.service";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        private readonly authService: AuthService
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: process.env.JWT_SECRET
        });
    }

    async validate(payload) {
        await this.authService.decipherAndValidateCredentials(
            this.authService.filterPayloadJWT<CredentialsDto>(payload)
        );

        return payload;
    }
}