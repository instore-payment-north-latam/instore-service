export const jwtFactory = {
    useFactory() {
        return {
            secret: process.env.JWT_SECRET,
            signOptions: {
                expiresIn: process.env.ACCESS_TOKEN_EXPIRES_IN
            }
        };
    }
};