import { Injectable } from '@nestjs/common';
import * as forge from "node-forge";
import { PRIVATE_KEY, PUBLIC_KEY } from '../../../config/auth';

@Injectable()
export class CipherService {
    decipherAsymmetric<T>(messageCipher: forge.Bytes): T {
        const privateKeyReference = forge.pki.privateKeyFromPem(PRIVATE_KEY);
        const message = privateKeyReference.decrypt(messageCipher);

        return JSON.parse(message);
    }

    cipherAsymmetric<T>(messageCipher: T): string {
        const privateKeyReference = forge.pki.publicKeyFromPem(PUBLIC_KEY);
        const message = privateKeyReference.encrypt(JSON.stringify(messageCipher));

        return message;
    }
}
