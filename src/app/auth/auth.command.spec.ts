import { Test, TestingModule } from '@nestjs/testing';
import { AuthCommand } from './auth.command';

describe('AuthCommand', () => {
    let controller: AuthCommand;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [AuthCommand],
        }).compile();

        controller = module.get<AuthCommand>(AuthCommand);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
