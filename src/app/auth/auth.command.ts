import { Command, Positional } from 'nestjs-command';
import { Injectable } from '@nestjs/common';
import { AuthService } from './auth.service';


@Injectable()
export class AuthCommand {
    constructor(private readonly authService: AuthService) { }

    @Command({
        command: 'token <apiKey>',
        describe: 'Crear nuevo token',
        autoExit: true
    })
    async createToken(
        @Positional({
            name: 'apiKey',
            describe: 'Nombre de API_KEY',
            type: 'string'
        })
        apiKey: string,
    ) {
        const data = await this.authService.buildToken(apiKey);
        console.log(data);
    }
}