import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { PassportModule } from "@nestjs/passport";
import { AuthService } from './auth.service';
import { AuthController } from "./auth.controller";
import { MongooseModule } from "@nestjs/mongoose";
import { AuthSchema } from "../schemas/auth.schema";
import { jwtFactory, JwtStrategy } from "./strategies/jwt";
import { AuthCommand } from "./auth.command";
import { CommandModule } from "nestjs-command";
import { CipherService } from './cipher/cipher.service';

@Module({
    imports: [
        CommandModule,
        MongooseModule.forFeature([
            { name: 'Auth', schema: AuthSchema, collection: "auth" }
        ]),
        PassportModule.register({
            defaultStrategy: "jwt"
        }),
        JwtModule.registerAsync(jwtFactory)
    ],
    controllers: [AuthController],
    providers: [JwtStrategy, AuthService, AuthCommand, CipherService],
    exports: [JwtStrategy, PassportModule]
})
export class AuthModule { }