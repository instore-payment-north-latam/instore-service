import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CredentialsDto } from '../dto/auth/credentials.dto';
import { Auth } from '../interfaces/auth.interface';
import { JwtPayload } from '../interfaces/jwt-payload.interface';
import { CipherService } from './cipher/cipher.service';

@Injectable()
export class AuthService {
    constructor(
        @InjectModel('Auth') private readonly authModel: Model<Auth>,
        private readonly jwtService: JwtService,
        private readonly cipherService: CipherService
    ) { }

    async createTokenJWT(credentials: CredentialsDto): Promise<string> {
        await this.decipherAndValidateCredentials(credentials);

        return this.jwtService.sign(credentials);
    }

    private async validateApiKeyAndApiToken(apiKey: string, apiToken: string) {
        const transaction = await this.authModel
            .findOne({
                apiKey,
                apiToken
            })
            .exec();

        if (!transaction) {
            throw new UnauthorizedException('Access token invalid');
        }
    }

    async decipherAndValidateCredentials(credentials: CredentialsDto): Promise<JwtPayload> {
        /**
         * Descifra la informacion
         */
        const { apiToken, apiKey } = this.cipherService.decipherAsymmetric<JwtPayload>(credentials.value);

        /**
         * Valida el apiToken y apiKey del cliente con el que esta registrado en la base de datos
         */
        await this.validateApiKeyAndApiToken(apiKey, apiToken);

        return { apiToken, apiKey };
    }

    async refreshToken(oldToken: string): Promise<string> {
        if (!oldToken) {
            throw new UnauthorizedException('Access token is not set');
        }

        /**
         * Valida si el JWT es valido
         */
        const payloadCredentials = this.validateToken(oldToken);

        /**
         * Desciframos la informacion
         */
        const { apiKey, apiToken } = this.cipherService.decipherAsymmetric<JwtPayload>(payloadCredentials.value);

        if (!apiKey || !apiToken) {
            throw new UnauthorizedException('Api token is invalid');
        }

        /**
         * Valida el apiToken y apiKey del cliente
         */
        await this.validateApiKeyAndApiToken(apiKey, apiToken);

        /**
         * Se crea un nuevo token
         */
        return this.createTokenJWT(payloadCredentials);
    }

    filterPayloadJWT<T>(payload: any): T {
        payload.exp && delete payload.exp;
        payload.iat && delete payload.iat;

        return { ...payload };
    }

    /**
     * 
     * @param token 
     * Validar si el JWT cumple con el formato
     */
    private validateToken(token: string): CredentialsDto {
        let payload = null;
        try {
            payload = this.jwtService.verify(token);
        } catch (err) {
            if (err?.message !== "jwt expired") {
                throw new UnauthorizedException('Token invalid');
            }

            payload = this.jwtService.decode(token);
        } finally {
            if (payload) {
                return this.filterPayloadJWT<CredentialsDto>(payload);
            }
        }
    }

    async buildToken(apiKeyParam: string) {
        const { apiToken, apiKey } = this.genereteApiKeyAndApiToken(apiKeyParam);
        const dataModel = {
            apiKey: apiKey,
            apiToken: apiToken
        };
        const newToken = await new this.authModel(dataModel);
        await newToken.save();
        return dataModel;
    }

    private genereteApiKeyAndApiToken(apiKey: string) {
        return {
            apiToken: this.rand() + this.rand(), // to make it longer
            apiKey
        };
    }

    private rand() {
        return Math.random().toString(36).substr(2); // remove `0.`
    }

}
