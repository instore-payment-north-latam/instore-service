import {
    Process,
    Processor,
    OnQueueActive,
    OnQueueStalled,
    OnQueueCompleted,
    OnQueueWaiting,
    OnQueueError, OnQueueFailed
} from "@nestjs/bull";
import { Job } from 'bull';
import { HttpService, Logger } from '@nestjs/common';
import { PaymentRequestService } from "../payment-request.service";
import { TransactionService } from "../../transaction/transaction.service";
import * as moment from "moment";
import * as Soap from 'soap';
import { Utils } from "../../shared/utils";
import { RedebanQueue } from "./redeban-queue.interface";

@Processor('redeban')
export class RedebanQueueConsumer {
    private readonly logger = new Logger(RedebanQueueConsumer.name);

    constructor(
        private httpService: HttpService,
        private paymentRequestService: PaymentRequestService,
        private transactionService: TransactionService) { }

    @Process()
    async checkResponse(job: Job<RedebanQueue>) {
        const jobData = <RedebanQueue>JSON.parse(JSON.stringify(job.data))
        let attempts = jobData.attempts;

        const client = await Soap.createClientAsync(jobData.url);

        const result = await client.LeerRespuestaPCAsync({ Terminal: jobData.terminal });
        let paymentResponse = this.getEmptyResult();

        if (result[0].LeerRespuestaPCResult !== '') {
            this.logger.debug(attempts);
            paymentResponse = this.getPaymentResponse(result[0].LeerRespuestaPCResult);

            await this.updateAndSendTransactionData(paymentResponse, jobData.paymentID);
            await this.paymentRequestService.removeJobFromQueue(job.name, { every: 10000, limit: 15, jobId: jobData.paymentID });

            return { done: true }
        }
        attempts += 1
        jobData.attempts = attempts;

        await this.paymentRequestService.updateJobOnQueue(jobData);
    }

    @OnQueueActive()
    onActive(job: Job) {
        this.logger.log(
            `Processing job ${job.id} of type ${job.name} with data ${job.data}...`,
        );
    }

    @OnQueueWaiting()
    onWaiting(job: Job) {
        this.logger.log(
            `Waiting job ${job.id} of type ${job.name} with data ${job.data}...`,
        );
    }

    @OnQueueStalled()
    onStalled(job: Job) {
        this.logger.log(
            `Stalled job ${job.id} of type ${job.name} with data ${job.data}...`,
        );
    }

    @OnQueueCompleted()
    async onCompleted(job: Job) {
        this.logger.log(
            `Completed job ${job.id} of type ${job.name} with data ${job.data}...`,
        );
    }

    @OnQueueError()
    async onError(error: Error) {
        this.logger.error(error)
    }

    @OnQueueFailed()
    async onFailed(job: Job<any>, e: Error) {
        if (e.message === "job stalled more than allowable limit") {
            const data = [];
            for (let i = 0; i < 16; i++) { data.push('timeout') }
            const paymentResponse = this.getPaymentResponse(data);
            await this.updateAndSendTransactionData(paymentResponse, job.data.paymentID)
            await this.paymentRequestService.removeJobFromQueue(job.name, { every: 10000, limit: 15, jobId: job.data.paymentID })
        }
    }

    getEmptyResult() {
        return {
            rawResponse: '',
            approveCode: '',
            cardNumbers: '',
            accountType: '',
            franchise: '',
            amount: 0,
            taxes: 0,
            impoConsumo: 0,
            receiptNumber: '',
            paymentID: '',
            installments: '',
            payeeTransactionId: '',
            terminalNumber: '',
            tenantNumber: '',
            date: '',
            hour: '',
            voucherNumber: '',
            operationType: ''
        };
    }

    getPaymentResponse(data) {
        const responseData = (data).split(',');
        return {
            rawResponse: data, // Respuesta
            approveCode: responseData[1].trim(), // Código de aprobación
            cardNumbers: responseData[2].trim(), // Número tarjeta
            accountType: responseData[3].trim(), // Tipo de cuenta (AH, CC, CR)
            franchise: responseData[4].trim(), // Franquicia
            amount: responseData[5].trim(), // Monto
            taxes: responseData[6].trim(), // IVA
            impoConsumo: responseData[7].trim(), // Base
            receiptNumber: responseData[8].trim(), // Impoconsumo
            paymentID: responseData[9].trim(), // Recibo
            installments: responseData[10].trim(), // Cuotas
            payeeTransactionId: responseData[11].trim(), // Consecutivo de transacción (RRN)
            terminalNumber: responseData[12].trim(), // Terminal
            tenantNumber: responseData[13].trim(), // Establecimiento
            date: moment(responseData[14].trim() + responseData[15].trim(), 'YYMDHm').toISOString(), // Fecha
            hour: responseData[15].trim(), // Hora
            voucherNumber: responseData[16].trim(), // # Bono / Voucher
            operationType: responseData[0].trim() // Operación
        }
    }

    async updateAndSendTransactionData(data, paymentID) {
        // Obtener los datos para actualizar el pago
        const paymentRequestDTO = await this.paymentRequestService
            .modelForUpdatePaymentRequest(
                data,
                paymentID,
                Utils.getStatus(data.approveCode)
            );
        const updatePaymentResponse = await this.paymentRequestService.updateService(paymentID, paymentRequestDTO);


        // Obtener la transacción, y agregar la respuesta para enviar al cliente
        const transactionData = await this.transactionService.getTransactionsByRef(paymentRequestDTO.receipt);
        transactionData.status = updatePaymentResponse.status;
        await this.transactionService.updateTransaction(transactionData._id, transactionData);

        /**
         * Se elimina las referencias del objecto Mongoose
         */
        const transaction = JSON.parse(JSON.stringify(transactionData));
        transaction.paymentResponse = paymentRequestDTO;
        // Enviar la transacción a los clientes conectados
        await this.httpService.post('https://pubsub.vtexnorthlatam.com:9850/pub?id=' + paymentID, transaction, { headers: { 'Content-type': 'application/json' } }).toPromise()
    }
}
