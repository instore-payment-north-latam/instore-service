export interface RedebanQueue {
    request: string;
    /**
     * Id del payment request
     */
    paymentID: string;
    terminal: string;
    url: string;
    attempts: number;
    date: string;
}