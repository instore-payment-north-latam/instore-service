import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { PaymentRequest } from "../interfaces/payment_request.interface";
import { PaymentRequestDto, RequestResultDto } from "../dto/transaction/create-payment-request.dto";
import { TransactionDto } from "../dto/transaction/transaction.dto";
import { EveryRepeatOptions, JobId, Queue } from 'bull';
import { InjectQueue } from "@nestjs/bull";
import * as moment from "moment";
import { DeviceService } from "../device/device.service";
import { PaymentFactory } from '../transaction/factories/payment-type/payment-factory';
import { Transaction } from '../shared/enums';
import { RedebanQueue } from './queues/redeban-queue.interface';
import { TransactionResponseRequestDto } from '../dto/transaction/transaction-response-request.dto';

@Injectable()
export class PaymentRequestService {

    constructor(
        @InjectModel('PaymentRequest') private readonly paymentRequestModel: Model<PaymentRequest>,
        @InjectQueue('redeban') private redebanQueue: Queue<RedebanQueue>,
        private deviceService: DeviceService,
        private paymentFactory: PaymentFactory
    ) { }

    async create(createTransactionDto: TransactionDto, operation: string): Promise<PaymentRequest> {
        const paymentRequestDTO = await this.getTransactionData(createTransactionDto, operation);

        this.validatePaymentUrl(createTransactionDto.paymentType, paymentRequestDTO.paymentUrl);

        const paymentRequest = await new this.paymentRequestModel(paymentRequestDTO);
        await paymentRequest.save();

        await this.paymentFactory.create(createTransactionDto.paymentType, paymentRequest);

        return paymentRequest;
    }

    private validatePaymentUrl(typePayment: Transaction.PAYMENT_TYPE, paymentUrl: string): void {
        if (
            typePayment === Transaction.PAYMENT_TYPE.WEB_SERVICE_REDEBAN &&
            !paymentUrl
        ) {
            throw new BadRequestException("Error payment url");
        }
    }

    async getService(id): Promise<PaymentRequest> {
        const service = await this.paymentRequestModel
            .findById(id)
            .exec();
        return service;
    }

    async updatePaymentRequest(transactionResponseRequestDto: TransactionResponseRequestDto): Promise<PaymentRequestDto> {
        const { idPaymentRequest, ...transactionResponse } = transactionResponseRequestDto.response;

        const date = transactionResponse.date
            ? moment(new Date(transactionResponse.date), 'YYMDHm').toISOString()
            : moment(new Date(), 'YYMDHm').toISOString()

        const paymentResponse: RequestResultDto = {
            rawResponse: transactionResponse.messageResponse || "",
            approveCode: transactionResponse.approveCode || "",
            cardNumbers: transactionResponse.cardNumbers || "",
            accountType: transactionResponse.accountType || "",
            franchise: transactionResponse.franchise || "",
            amount: transactionResponse.amount || 0,
            taxes: transactionResponse.taxes || 0,
            impoConsumo: 0,
            receiptNumber: transactionResponse.receiptNumber || "",
            paymentID: transactionResponse.confirmNumber || "",
            installments: transactionResponse.numberInstallments || "",
            payeeTransactionId: "",
            terminalNumber: "",
            tenantNumber: "",
            date,
            hour: "",
            voucherNumber: "",
            operationType: transactionResponse.operationType || ""
        };

        const paymentRequest = await this.modelForUpdatePaymentRequest(
            paymentResponse,
            idPaymentRequest,
            transactionResponseRequestDto.status
        );
        const referencePayemntRequest = await this.updateService(idPaymentRequest, paymentRequest);

        return referencePayemntRequest.toObject<PaymentRequestDto>();
    }

    async updateService(id: string, paymentRequestDTO: PaymentRequestDto): Promise<PaymentRequest> {
        const update = await this.paymentRequestModel
            /* @ts-ignore */
            .findByIdAndUpdate(id, paymentRequestDTO, { new: true })
        return update;
    }

    /**
     * @param response
     * @param idPaymentRequest Id de PaymentRequest
     * @returns 
     */
    async modelForUpdatePaymentRequest(response: RequestResultDto, idPaymentRequest: string, status: Transaction.STATUS): Promise<PaymentRequestDto> {
        const paymentRequest = await this.getService(idPaymentRequest);

        if (!paymentRequest) {
            throw new NotFoundException('Payment request not found');
        }

        return {
            terminal: paymentRequest.terminal,
            paymentUrl: paymentRequest.paymentUrl,
            operation: paymentRequest.operation,
            amount: paymentRequest.amount,
            tip: paymentRequest.tip,
            tax: paymentRequest.tax,
            invoice: paymentRequest.invoice,
            taxReturnBase: paymentRequest.taxReturnBase,
            taxRateBase: paymentRequest.taxRateBase,
            cashierID: paymentRequest.cashierID,
            impoConsumo: paymentRequest.impoConsumo,
            impoConsumoBase: paymentRequest.impoConsumoBase,
            receipt: paymentRequest.receipt,
            status,
            response,
            /**
             * @todo validar propiedad (attempts) no se esta utilizando
             */
            /* @ts-ignore */
            responseAttempts: response.attempts || 0,
            createdAt: paymentRequest.createdAt,
            updatedAt: moment().toISOString()
        }
    }

    async getTransactionData(createTransactionDto: TransactionDto, operation: string): Promise<PaymentRequestDto> {
        /**
         * Se calcula los impuestos
         */
        const taxes = Math.round(createTransactionDto.amount - (createTransactionDto.amount / (createTransactionDto.taxRate + 1)));
        const taxReturnBase = Math.round(createTransactionDto.amount / (createTransactionDto.taxRate + 1));
        const taxBase = createTransactionDto.taxRate * 100;

        const device = await this.deviceService.getDevice(createTransactionDto.deviceId);

        this.deviceService.validateDevice(device);

        const payment = {
            /**
             * || ""
             * @description Empty string is added because the A920 devices
             * do not have the properties "terminal" and "url"
             */
            terminal: device.paymentMethods.redeban.terminal || "",
            paymentUrl: device.paymentMethods.redeban.url || "",

            operation: operation,
            amount: createTransactionDto.amount,
            tip: 0,
            tax: taxes,
            invoice: createTransactionDto.orderId,
            taxReturnBase: taxReturnBase,
            taxRateBase: taxBase,
            cashierID: '01', /*TODO: ¿Esto para que es?*/
            impoConsumo: 0,
            impoConsumoBase: 0,
            receipt: createTransactionDto.paymentId,
            status: Transaction.STATUS.PENDING,
            response: {
                rawResponse: '',
                approveCode: '',
                cardNumbers: '',
                accountType: '',
                franchise: '',
                amount: 0,
                taxes: 0,
                impoConsumo: 0,
                receiptNumber: '',
                paymentID: '',
                installments: '',
                payeeTransactionId: '',
                terminalNumber: '',
                tenantNumber: '',
                date: '',
                hour: '',
                voucherNumber: '',
                operationType: '',
            },
            responseAttempts: 0,
            createdAt: moment().toISOString(),
            updatedAt: moment().toISOString()
        }

        return payment;
    }

    // Todo: pagos divididos
    async checkIfTransactionIsAlreadyPayed(invoice) {
        const services = await this.paymentRequestModel
            .find({
                invoice
            })
            .exec();
        const payed = services.filter((o) => {
            return o.status === 'approved'
        })
        return (payed.length > 0);
    }

    async updateJobOnQueue(data: RedebanQueue) {
        const jobs = await this.redebanQueue.getJobs(["waiting", "active", "delayed", "paused"]);

        for (let i = 0; i < jobs.length; i++) {
            const job = jobs[i];

            if (job.data.paymentID === data.paymentID) {
                await job.update(data);
            }
        }
    }

    async removeJobFromQueue(name: string, repeat: EveryRepeatOptions & { jobId?: JobId }) {
        return await this.redebanQueue.removeRepeatable(name, repeat);
    }
}
