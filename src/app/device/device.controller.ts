import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    NotFoundException,
    Param,
    Post,
    Put,
    Query,
    Res,
    UseInterceptors
} from '@nestjs/common';
import { DeviceService } from "./device.service";
import { ValidateObjectId } from "../shared/pipes/validate-object-id.pipes";
import * as moment from 'moment';
import { ApiBearerAuth, ApiParam, ApiQuery, ApiResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { CreateDeviceDto } from '../dto/device/create_device.dto';
import { SerializerInterceptor } from '../utils/interceptors/serializer.interceptor';
import { DeviceDto } from '../dto/device/device.dto';
import { throwErrorIfEmptyOrNil } from '../utils/validation.utils';
import { NotFoundExceptionDto } from '../dto/shared/not-found-exception.dto';
import { UnauthorizedExceptionDto } from '../dto/shared/unauthorized-exception.dto';
import { Schema } from 'mongoose';
import { validate } from 'class-validator';
import { BadRequestException } from '@nestjs/common';
import { ValidateUuid } from '../shared/validators/validate-uuid';
import { HttpExceptionDto } from '../dto/shared/http-exception.dto';

/**
 * @todo Add auth
 */
// @UseGuards(AuthGuard())
@ApiTags('Devices')
@ApiBearerAuth()
@ApiUnauthorizedResponse({ description: "Unauthorized", type: UnauthorizedExceptionDto })
@Controller('devices')
export class DeviceController {
    constructor(private deviceService: DeviceService) { }

    @Post('/')
    @HttpCode(HttpStatus.OK)
    @UseInterceptors(new SerializerInterceptor(DeviceDto, 'Device saved'))
    @ApiResponse({ status: HttpStatus.OK, type: DeviceDto, description: "Device saved" })
    addDevice(@Body() createDeviceDTO: CreateDeviceDto) {
        return this.deviceService.createDevice(createDeviceDTO);
    }

    @Get('/')
    @UseInterceptors(new SerializerInterceptor(DeviceDto, 'Devices retrieved'))
    @ApiResponse({ status: HttpStatus.OK, type: DeviceDto, isArray: true })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, type: NotFoundExceptionDto, description: "No devices found" })
    getDevices() {
        return this.deviceService.getDevices()
            .then()
            .then(throwErrorIfEmptyOrNil(new NotFoundException('No devices found')))
    }

    @Get('/:idDevice')
    @UseInterceptors(new SerializerInterceptor(DeviceDto, 'seller retrieved'))
    @ApiParam({ name: 'idDevice' })
    @ApiResponse({ status: HttpStatus.OK, type: DeviceDto })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, type: NotFoundExceptionDto, description: "Device not found" })
    getDevice(@Param('idDevice', new ValidateObjectId()) idDevice) {
        return this.deviceService.getDevice(idDevice)
            .then(throwErrorIfEmptyOrNil(new NotFoundException('Device not found')));
    }

    @Get('/count/:idSeller')
    @ApiParam({ name: 'idSeller' })
    @ApiResponse({ status: HttpStatus.OK, description: "sucess", type: Number })
    async countBySeller(@Param('idSeller', new ValidateObjectId()) idSeller) {
        const count = await this.deviceService.countBySeller(idSeller)
        return {
            message: 'sucess',
            data: count
        };
    }

    @Get("list/:sellerId")
    @ApiParam({ name: "sellerId" })
    @ApiQuery({ name: "name", required: false })
    @ApiResponse({ status: HttpStatus.OK, description: "Devices retrieved", type: DeviceDto, isArray: true })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, type: NotFoundExceptionDto, description: "No devices found" })
    @ApiResponse({ status: HttpStatus.BAD_REQUEST, type: HttpExceptionDto, description: "SellerId: Value must be a mongodb id" })
    async getDevicesBySeller(
        @Res() res,
        @Param("sellerId") sellerId: string,
        @Query("name") text?: string
    ) {
        const uuid = new ValidateUuid();
        uuid.value = sellerId;
        const errors = await validate(uuid);

        if (errors.length > 0) {
            throw new BadRequestException("SellerId: Value must be a mongodb id");
        }

        const devices = await this.deviceService.getSellerDevices(sellerId);

        if (devices.length == 0) {
            throw new NotFoundException('No devices found');
        }

        const devicesFiltered = this.deviceService.filterDevices(text, devices);

        return res.status(HttpStatus.OK).json({
            message: 'Devices retrieved',
            data: devicesFiltered
        });
    }

    @Get('listbyname/:name')
    @ApiParam({ name: "name" })
    @ApiResponse({ status: HttpStatus.OK, type: DeviceDto, isArray: true })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, type: NotFoundExceptionDto, description: "No devices found" })
    async getDevicesBySellerName(@Param('name') name: string) {
        let devices = null
        devices = await this.deviceService.getSellerDevicesBySellerName(name);
        if (devices.length == 0) {
            throw new NotFoundException('No devices found');
        }
        return devices;
    }

    @Put('/:idDevice')
    @UseInterceptors(new SerializerInterceptor(DeviceDto, 'Device updated'))
    @ApiParam({ name: 'idDevice' })
    @ApiResponse({ status: HttpStatus.OK, type: DeviceDto, description: "Device updated" })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, type: NotFoundExceptionDto, description: "Device not found" })
    updateDevice(
        @Body() createDeviceDTO: CreateDeviceDto,
        @Param('idDevice', new ValidateObjectId()) idDevice: string,
    ) {
        createDeviceDTO.updatedAt = moment().toISOString();
        return this.deviceService.updateDevice(idDevice, createDeviceDTO)
            .then(throwErrorIfEmptyOrNil(new NotFoundException('Device not found')));
    }

    @Delete('/:idDevice')
    @ApiParam({ name: 'idDevice' })
    @ApiResponse({ status: HttpStatus.OK, description: "Device deleted" })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, type: NotFoundExceptionDto, description: "Device not found" })
    async deleteDevice(@Param('idDevice', new ValidateObjectId()) idDevice: string) {
        await this.deviceService.deleteDevice(idDevice)
            .then(throwErrorIfEmptyOrNil(new NotFoundException('Device not found')));
        return {
            message: 'Device deleted'
        };
    }
}
