import { Injectable, NotFoundException } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from "@nestjs/mongoose";
import { Device } from '../interfaces/device.interface'
import { CreateDeviceDto } from "../dto/device/create_device.dto";
import { SellerService } from "../seller/seller.service";
import { Seller } from '../interfaces/seller.interface';
import { Utils } from '../shared/utils';

@Injectable()
export class DeviceService {
    constructor(
        @InjectModel('Device') private readonly deviceModel: Model<Device>,
        private sellerService: SellerService
    ) { }

    async createDevice(createDeviceDTO: CreateDeviceDto): Promise<Device> {
        const newDevice = await new this.deviceModel(createDeviceDTO)
        return newDevice.save();
    }

    async getDevice(id): Promise<Device> {
        const device = await this.deviceModel
            .findById(id)
            .exec();

        return device;
    }

    async getDevices(): Promise<Device[]> {
        const devices = await this.deviceModel
            .find()
            .exec()
        return devices
    }

    async getSellerDevices(sellerId: string): Promise<Device[]> {
        const devices = await this.deviceModel
            .find({
                _sellerID: sellerId
            })
            .exec();
        return devices
    }

    public filterDevices(text: string, devices: Device[]): Device[] {
        if (!text) {
            return devices;
        }

        return devices.filter(({ deviceLabel, deviceModel, deviceType }) => {
            const currentDeviceName = Utils.clearText(deviceLabel) + Utils.clearText(deviceModel) + Utils.clearText(deviceType);

            return currentDeviceName.includes(Utils.clearText(text));
        });
    }

    async getSellerDevicesBySellerName(name): Promise<Device[]> {
        const seller = await this.sellerService.getSeller(name);
        if (seller) {
            return await this.getSellerDevices(seller.id);
        }
        return []
    }

    async countBySeller(idSeller): Promise<number> {
        const count = await this.deviceModel
            .countDocuments({
                _sellerID: idSeller
            })
            .exec();
        return count;
    }

    async updateDevice(id, createDeviceDTO: CreateDeviceDto): Promise<Device> {
        const device = await this.deviceModel
            .findByIdAndUpdate(id, createDeviceDTO, { new: true })
            .exec();
        return device;
    }

    async deleteDevice(id): Promise<any> {
        const deleted = await this.deviceModel
            .findByIdAndRemove(id)
            .exec();
        return deleted;
    }

    async getCountBySeller(sellers: Seller[]): Promise<Array<Seller & { devicesCount: number }>> {
        const pendingPromises = sellers.map(async (seller) => {
            const count = await this.countBySeller(seller._id);
            return {
                devicesCount: count || 0,
                ...seller.toObject()
            } as Seller & { devicesCount: number };
        });

        return await Promise.all(pendingPromises);
    }

    validateDevice(device: Device): void {
        if (!device) {
            throw new NotFoundException('Device not found');
        }
    }
}
