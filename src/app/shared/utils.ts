import { Transaction } from "./enums";

export class Utils {
    static getStatus(approveCode: string): Transaction.STATUS {
        if (approveCode === '') {
            return Transaction.STATUS.REJECT;
        } else if (approveCode !== '') {
            return Transaction.STATUS.APPROVED;
        } else {
            return Transaction.STATUS.CANCELLED;
        }
    }

    static clearText(text: string): string {
        /**
         * Remove Accents
         */
        const textWithoutAccents = text.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
        return textWithoutAccents.toLowerCase().trim();
    }
}