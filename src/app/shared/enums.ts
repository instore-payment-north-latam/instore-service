/* eslint-disable @typescript-eslint/no-namespace */

export namespace Transaction {
    export enum PAYMENT_TYPE {
        WEB_SERVICE_REDEBAN = "webservice-redeban-pay",
        DEEP_LINKING_REDEBAN = "deeplinking-redeban"
    };

    export enum STATUS {
        APPROVED = "approved",
        REJECT = "reject",
        CANCELLED = "cancelled",
        PENDING = "pending"
    }
}