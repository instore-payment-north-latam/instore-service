import { IsMongoId } from "class-validator";

export class ValidateUuid {
    @IsMongoId()
    value: string;
}

