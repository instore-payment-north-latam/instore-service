import { Transform } from "class-transformer";

/**
 * @todo  Validate because "v=true", validate enableImplicitConversion in main.ts
 */
export function ToBoolean() {
    return Transform(v => ["1", 1, "true", true].includes(v));
}