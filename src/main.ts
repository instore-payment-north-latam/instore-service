import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
    "preflightContinue": false,
    "optionsSuccessStatus": 204
  });

  app.useGlobalPipes(new ValidationPipe({
    /**
     * Si se establece en verdadero, el validador omitirá la validación de todas
     * las propiedades que faltan en el objeto de validación.
     * Example
     * Class A {
     *  @IsDefined()
     *  @IsString()
     *  name: string;
     * }
     * -Disabled skipMissingProperties = false
     *
     * "name should not be null or undefined",
     * "name must be a string",
     * -Enable skipMissingProperties = true
     * 
     * "name should not be null or undefined",
     */
    skipMissingProperties: true,
    transformOptions: {
      /**
       * More info https://github.com/typestack/class-transformer#enforcing-type-safe-instance
       */
      excludeExtraneousValues: true,
      /**
       * More info https://github.com/typestack/class-transformer#implicit-type-conversion
       */
      enableImplicitConversion: true
    }
  }));

  const options = new DocumentBuilder()
    .setTitle('IN-STORE (Transactions service)')
    .setDescription('Transactions service')
    .setVersion('1.0.0')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('documentation', app, document);

  await app.listen(5000);
}
bootstrap();
