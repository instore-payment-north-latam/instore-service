import * as fs from "fs";

const PRIVATE_KEY = fs.readFileSync(__dirname + "/privKey.pem").toString();
const PUBLIC_KEY = fs.readFileSync(__dirname + "/privKey.pem").toString();


export {
    PRIVATE_KEY,
    PUBLIC_KEY
};