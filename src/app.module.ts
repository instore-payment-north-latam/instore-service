import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TransactionModule } from "./app/transaction/transaction.module";
import { SettingModule } from "./app/settings/settings.module";
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './app/auth/auth.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TransactionModule,
    AuthModule,
    SettingModule,
    MongooseModule.forRoot(process.env.MONGO_DB, { useNewUrlParser: true })
  ]
})
export class AppModule { }
